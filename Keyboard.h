#pragma once
#include "AgnosiaDependencies.h"

#define QWERTY_A 'Q'
#define QWERTY_B 'B'
#define QWERTY_C 'C'
#define QWERTY_D 'D'
#define QWERTY_E 'E'
#define QWERTY_F 'F'
#define QWERTY_G 'G'
#define QWERTY_H 'H'
#define QWERTY_I 'I'
#define QWERTY_J 'J'
#define QWERTY_K 'K'
#define QWERTY_L 'L'
#define QWERTY_M ';'
#define QWERTY_N 'N'
#define QWERTY_O 'O'
#define QWERTY_P 'P'
#define QWERTY_Q 'A'
#define QWERTY_R 'R'
#define QWERTY_S 'S'
#define QWERTY_T 'T'
#define QWERTY_U 'U'
#define QWERTY_V 'V'
#define QWERTY_W 'Z'
#define QWERTY_X 'X'
#define QWERTY_Y 'Y'
#define QWERTY_Z 'W'
#define QWERTY_0 '0'
#define QWERTY_1 '1'
#define QWERTY_2 '2'
#define QWERTY_3 '3'
#define QWERTY_4 '4'
#define QWERTY_5 '5'
#define QWERTY_6 '6'
#define QWERTY_7 '7'
#define QWERTY_8 '8'
#define QWERTY_9 '9'
#define QWERTY_² '`'


#define AZERTY_A 'A'
#define AZERTY_B 'B'
#define AZERTY_C 'C'
#define AZERTY_D 'D'
#define AZERTY_E 'E'
#define AZERTY_F 'F'
#define AZERTY_G 'G'
#define AZERTY_H 'H'
#define AZERTY_I 'I'
#define AZERTY_J 'J'
#define AZERTY_K 'K'
#define AZERTY_L 'L'
#define AZERTY_M 'M'
#define AZERTY_N 'N'
#define AZERTY_O 'O'
#define AZERTY_P 'p'
#define AZERTY_Q 'Q'
#define AZERTY_R 'R'
#define AZERTY_S 'S'
#define AZERTY_T 'T'
#define AZERTY_U 'U'
#define AZERTY_V 'V'
#define AZERTY_W 'W'
#define AZERTY_X 'X'
#define AZERTY_Y 'Y'
#define AZERTY_Z 'Z'
#define AZERTY_0 '0'
#define AZERTY_1 '1'
#define AZERTY_2 '2'
#define AZERTY_3 '3'
#define AZERTY_4 '4'
#define AZERTY_5 '5'
#define AZERTY_6 '6'
#define AZERTY_7 '7'
#define AZERTY_8 '8'
#define AZERTY_9 '9'
#define AZERTY_² '²'


#define AmountLinekeys 32
#define AmountMouseKeys 3


class Agnosia::input {
public:
	char key;
	bool isActive;
	bool wasActive;

	int getInputState(); //Returns 0 if inactive, 1 if newly inactive, 2 if active, 3 if newly active.
	input();
};

class Agnosia::mouseInputs {
	Agnosia::input keys[3];

public:
	void update();
	int getInputState(int); //Input 0 for left click, 1 for right click, 2 for middle mouse button. Returns 0 if inactive, 1 if newly inactive, 2 if active, 3 if newly active.

	mouseInputs();
};

class Agnosia::inputBoard {
private:
	int dispositionType; //If 0 -> QWERTY, if 1 -> AZERTY, if 2 -> Personnalized. Remembers the "Personnalized settings.
	Agnosia::input keys[32]; //32 for the 32 line inputs
	Agnosia::mouseInputs mouse;

public:
	void update(); //Automatically calls mouse.update() too. Call once only per loops (frames) before checking inputs.
	int getKeyboardInput(int); //Returns 0 if inactive, 1 if newly inactive, 2 if active, 3 if newly active.
	int getMouseInput(int); //Returns 0 if inactive, 1 if newly inactive, 2 if active, 3 if newly active.
	void changeKeyValue(char);
	void replaceKey();

	inputBoard();
};