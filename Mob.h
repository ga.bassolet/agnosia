#pragma once
#include "Jinko.h"
#include "Geometry.h"
#include "Agnosia.h"
#include "GeneralFunctions.h"
#include "Map.h"

class Agnosia::Mob {
private:
	std::chrono::system_clock::time_point lastUpdateTime;
public:
	std::string name;
	std::string description;

	// Stats
	int maxHealth;
	int health;

	int strength; // Blunt CqC damage, also affects some ranged weapons (like bows)
	int spellAmplification; // Affects damage made by spells (like AP in LoL)
	int armor; // Blocks a flat amount of blunt damage
	int magicResistance; // Blocks a flat amount of magic damage
	double attackSpeed; // allows multiple attacks per turn


	double attackSpeedDelta; // If AS = 2.6 and last turn, the player attacked twice, this turn's total AS = AS + delta (0.6).

	double hygdRegen;
	double staminaRegen;
	double healthRegen;

	int hygd; //mana
	int stamina;

	int initiative;
	int speed;


	int height; //height of the location

	Jinko::point position;
	Jinko::vector orientation;

	void update(std::chrono::system_clock::time_point);
	void move(std::chrono::system_clock::time_point, Jinko::vector, Agnosia::Map*);
	void init(std::chrono::system_clock::time_point);
	Mob();
};