#pragma once
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "MSVCRTD.lib")
#pragma comment(lib, "Dwrite")

// Windows Header Files:
#include <windows.h>

// C RunTime Header Files:
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <wchar.h>
#include <math.h>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

#include <string>

#include <chrono>
#include <ctime>

#include <iostream>
#include <filesystem>
#include <fstream>

// Personal stuff :
#include "JinkoDependencies.h"
#include "AgnosiaDependencies.h"

#define InitialWindowSizeX 1920     //1366
#define InitialWindowSizeY 1080     //768

#define maxElementsInMenu 100
#define maxCharaInButton 50

#define maxUIelementsInAnimation 10
#define maxAnimationsInUIcollection 10

// Function for releasing interface :
template<class Interface>
inline void SafeRelease(Interface** ppInterfaceToRelease) {
    if (*ppInterfaceToRelease != NULL)
    {
        (*ppInterfaceToRelease)->Release();

        (*ppInterfaceToRelease) = NULL;
    }
}



// Debug macro :
#ifndef Assert
#if defined( DEBUG ) || defined( _DEBUG )
#define Assert(b) do {if (!(b)) {OutputDebugStringA("Assert: " #b "\n");}} while(0)
#else
#define Assert(b)
#endif //DEBUG || _DEBUG
#endif

// Debug macro :
#ifndef HINST_THISCOMPONENT
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#define HINST_THISCOMPONENT ((HINSTANCE)&__ImageBase)
#endif


//Part 3

class Jinko::Application {
public:
    Application();
    ~Application();

    // Register the window class and call methods for instantiating drawing resources
    HRESULT Initialize(std::string, std::string, int, int);
    HRESULT InitializeLayered(std::string, std::string, int, int);
    // (wchar_t* appName, wchar_t* windowName, int sizeX, int sizeY, DWORD windowStyle)

    void resizeRenderTargetToWindowSize();
    void ResizeWindow(int x, int y);
    void ResizeAll(int x, int y);

    void actualizeWindow(UIcollection*);

    // Tests :
    void RunMessageLoop();
    void dungeonGenerationTest1();

    // Game functions :
    void mainMenu();

    // Some Functions :
    bool WriteTextOnDesktop(std::string);
    void writeTextOnScreenLoopWithWindowHandle(std::string text, int time);
    bool writeTextOnScreen(std::string);

    // Useful Functions :
    void show();
    void hide();


private:
    // Initialize device-independent resources.
    HRESULT CreateDeviceIndependentResources();

    // Initialize device-dependent resources.
    HRESULT CreateDeviceResources();

    // Release device-dependent resource.
    void DiscardDeviceResources();

    // Draw content.
    HRESULT OnRender();

    // Resize the render target.
    void OnResize(
        UINT width,
        UINT height
    );

    // The windows procedure.
    static LRESULT CALLBACK WndProc(
        HWND hWnd,
        UINT message,
        WPARAM wParam,
        LPARAM lParam
    );
    //Jinko::Progression currentProgression;
    //Jinko::Progression a;

    // Layered window stuff
    HRESULT CreateDeviceResourcesLayered();
    void DiscardDeviceResourcesLayered();

    HWND m_hwnd;
    ID2D1Factory* m_pDirect2dFactory;
    IDWriteFactory* m_pD2dWriteFactory;
    IDWriteTextFormat* MainMenuMainButtonsTextFormat;
    IDWriteTextFormat* m_pNotificationTextFormat;
    ID2D1HwndRenderTarget* m_pRenderTarget;
    ID2D1SolidColorBrush* m_pLightSlateGrayBrush;
    ID2D1SolidColorBrush* m_pCornflowerBlueBrush;
    ID2D1SolidColorBrush* m_pBgGreyBrush;

    UIcollection* windowHandle;
    UIcollection* settingsMenu;
    UIcollection* activeMenu;
    DWORD windowStyle;
};

/*
activeMenu value : (deprecated?)
    0 : N/A
    1 : Main Menu (Play, Edit, Options)
    2 : Main Edit Menu
    3 : Chapter edit submenu
    4 : Text edit submenu
*/


class Jinko::UIelement {
private:
    unsigned int alignement = 0;
    /*  0x00 = None,
        0x01 = up,
        0x02 = down,
        0x04 = left,
        0x08 = right,
        0x10 = centered.
        /!\ These are mostly unused.
    */
    POINT position = { 0, 0 }; // Relative to alignment.
    POINT currentPosition; // Is updated everytimes the element is drawn
    POINT size;

    std::string text; // To write somewhere, depending on the style.
    unsigned int style;
    unsigned int type;

    DWORD windowStyle;
    /*
    Type :
        0 : Basic computed stuff
            Styles :
                0 : don't draw anything, but still generates a rectangular collision section, depending on the size and the position.
                1 : basic button
                2 : Exit button (cannot be moved or changed. Only one displayed element of this type is useful)
                3 : Popup text window
                4 : Popup close button
                5 : Draws a text at target location of desired size (size), font (fontName) and color (colorValue). Uses fvalue1 as the font size (if fvalue1 = 0, default value of 13 is used).
                6 : Draws a text at target location of desired size (size), font (fontName) and color (colorValue) progressively. Uses value1, incrementor1 and clockValue1 to determine what to write. Uses (_shouldWrite) to determine if it should continue writing.
                7 : Draws a pixelized screen of (colorValue) color with each pixel having a random opacity from 0 to (opacityFactor1). Uses (value1) to determine the size of the pixels.
                8 : Draws a pixelized screen like (7) with the center being more visible than the exterior, using (value2/1000) to determine how much of the center it should show.

        1 : Image Handler
            Styles :
                0 : Draws an image at target location (position) and generate its collision zone.
                1 : Draws the image at target location (position) from ressources

        2 : Animation

        4 : RenderTarget Transformation effects
            Note : Requires to have reset the render target at least once each frames.
            Styles :
                0 : Reset the RenderTarget to normal.
                1 : Cylinder effect (For RTF1).

        5 : RTF1
            Styles :

        6 : Visual Novel
            Styles :
                0 : Draws the (text) on the screen, using (value1) as the time to wait before writing the next character in ms,
    */
    bool _isVoid;

    std::chrono::system_clock::time_point clockValue1;
    std::chrono::system_clock::time_point clockValue2;
    std::chrono::system_clock::time_point timeOffset;
    bool _shouldWrite;


    // Adress of a file to handle (like the image path).
    std::string adress;
    DWORD handleElement;
    ID2D1Bitmap* image = nullptr;
    ID2D1Factory* m_pDirect2dFactory;
    IDWriteFactory* m_pD2dWriteFactory;
    IWICImagingFactory* pIWICFactory;
    IDWriteTextFormat* MainMenuMainButtonsTextFormat;
    IDWriteTextFormat* textFormat;
public:
    bool bool1 = false;
    bool bool2 = false;
    float opacityFactor1 = 1;

    bool _jobDone = false;
    int characterPosition;

    int value1; //Used in "UIcollection.poptext()" to move the text displayed. Also in other functions in UIelement.
    int value2;
    int value3;
    int value4;
    int value5;
    float fvalue1;
    int incrementor1;
    wchar_t fontName = *L"Gabriola";
    unsigned int colorValue;


    void resize(POINT);
    void resize(int, int);
    std::string getText();
    unsigned int getStyle();
    unsigned int getAlignement();
    bool get_isVoid();

    void reText(std::string);
    void reStyle(unsigned int);
    void reType(unsigned int);
    void re_isVoid(bool);

    void startWriting();
    void stopWriting();

    bool updatePosition(HWND);
    bool drawElement(HWND, ID2D1HwndRenderTarget*);

    void setImagePath(std::string);
    std::string getImagePath();

    void setNewRotationValueMenuRPG1(int);

    void setHandleElement(DWORD);

    void rePosition(POINT, unsigned int = 0);
    void rePosition(int, int, unsigned int = 0);
    void reRelativePosition(POINT);
    void reRelativePosition(int = 0, int = 0);

    RECT getArea();

    void hide();
    void show();
    bool _isDisplayed();

    bool check_isHovered(HWND, ID2D1HwndRenderTarget*, POINT);

    void init();

    bool _mustDisplay; // Wether or not the element must be drawn
    RECT activeArea;

    HRESULT CreateDeviceIndependentResources();


    void generateRectangleCollisionSection();
    void generateRectangleCollisionSection(int, int, int, int);

    bool drawBasicButton(HWND, ID2D1HwndRenderTarget*);
    bool drawExitButton(HWND, ID2D1HwndRenderTarget*);
    bool drawPopupTextWindow(HWND, ID2D1HwndRenderTarget*);
    bool drawPopupCloseButton(HWND, ID2D1HwndRenderTarget*);
    bool drawText(HWND, ID2D1HwndRenderTarget*);
    bool drawTextProgressively(HWND, ID2D1HwndRenderTarget*);
    bool drawPixelizedBlackFrame(HWND, ID2D1HwndRenderTarget*);
    bool drawImageFromHandle(HWND, ID2D1HwndRenderTarget*);
    bool drawImage(HWND, ID2D1HwndRenderTarget*);

    // RENDERTARGET TRANSFORMATION EFFECTS FUNCTIONS :
        // Check above for full descriptions. Type : 4, Style   --------------------------->     Below
    bool renderTargetTransformationReset(HWND, ID2D1HwndRenderTarget*);                     // 0
    bool renderTargetTransformationCylinderEffect(HWND, ID2D1HwndRenderTarget*);
    // END OF RTF1 FUNCTIONS


    // RTF1 FUNCTIONS :
        // Check above for full descriptions. Type : 5, Style   --------->   Below

    // END OF RTF1 FUNCTIONS

    // VISUAL NOVEL FUNCTIONS :
        // Check above for full descriptions. Type : 5, Style   --------->   Below
    bool displayTextVN1(HWND, ID2D1HwndRenderTarget*);                  // 0
// END OF VISUAL NOVEL FUNCTIONS

    bool drawAnimation(HWND, ID2D1HwndRenderTarget*, int);

    void affectWindowStyle(DWORD);

    UIcollection* owner;

    UIelement();
    ~UIelement();
    UIelement(POINT, unsigned int = 0);
};


class Jinko::UIcollection {
private:
    UIelement elements[maxElementsInMenu];
    bool _animate[maxAnimationsInUIcollection];
    bool _stopAnimations[maxAnimationsInUIcollection];
    std::chrono::system_clock::time_point clockValues[maxAnimationsInUIcollection];
    std::chrono::system_clock::time_point timeOffset[maxAnimationsInUIcollection];
    UIelement animation0[maxUIelementsInAnimation];
    UIelement animation1[maxUIelementsInAnimation];
    UIelement animation2[maxUIelementsInAnimation];
    UIelement animation3[maxUIelementsInAnimation];
    UIelement animation4[maxUIelementsInAnimation];
    UIelement animation5[maxUIelementsInAnimation];
    UIelement animation6[maxUIelementsInAnimation];
    UIelement animation7[maxUIelementsInAnimation];
    UIelement animation8[maxUIelementsInAnimation];
    UIelement animation9[maxUIelementsInAnimation];
    HWND m_Hwnd;
    ID2D1SolidColorBrush* m_pBgBrush;
    ID2D1HwndRenderTarget* m_pRenderTarget;
    bool _mustDisplay = true;
    bool _drawBg;

    DWORD windowStyle;

public:

    int _isHovered[maxElementsInMenu];
    bool displayElements(HWND);
    UIelement getElement(int);
    UIelement* getElementPointer(int);
    void addElement(UIelement);
    void insertElement(UIelement, int);
    void removeElement(int);
    bool drawElements(HWND, ID2D1HwndRenderTarget*);
    bool checkIfHovered(POINT);
    void affectWindow(HWND);

    void setBgBrush(ID2D1SolidColorBrush*);

    void hide();
    void show();
    bool isDisplayed();
    void _doDrawBg(bool);

    void refreshElementsOwner();

    void init(ID2D1HwndRenderTarget*);
    void init(ID2D1HwndRenderTarget*, HWND);

    void initAnim();

    void popText(std::string);
    void popMoreText(std::string);

    // This function requires to have a set of UIelements within a single UIelement[], with each of them having been configured to display an image. It must be terminated by a UIelement with _isVoid = true.
    // int for draw order (places the animation at the position 'pos' in elements[]). if _insert = false, deletes the element at the position 'pos' and replaces it completely by the animation.
    // RECT activeZone is used to determine where the animation's active area is.
    bool animateCollection(int collection, RECT activeZone = { 0,0,0,0 }, int speed = 1000, int pos = -1, bool _insert = true);
    //bool animateCollectionOpti(UIelement*, int speed = 1000, int pos = -1, bool _insert = true);
    bool addAnimationElement(UIelement element, int collection);
    void deleteAnimation(int collection = -1);
    bool drawAnimation(HWND, ID2D1HwndRenderTarget*, int, int);

    void affectWindowStyle(DWORD);

    void moveRelativePosition(POINT);
    void moveRelativePosition(int, int);

    int owner;              // 0 = N/A

    UIcollection(HWND, ID2D1HwndRenderTarget*, DWORD);
    UIcollection(HWND, ID2D1HwndRenderTarget*, ID2D1SolidColorBrush*, DWORD);
    UIcollection();
    ~UIcollection();
};
