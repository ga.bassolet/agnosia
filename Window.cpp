#include "Window.h"
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "winmm.lib")

using namespace std;
using namespace Jinko;
namespace fs = std::filesystem;

Application::Application() :
    m_hwnd(NULL),
    m_pDirect2dFactory(NULL),
    m_pRenderTarget(NULL),
    m_pLightSlateGrayBrush(NULL),
    m_pCornflowerBlueBrush(NULL)
{
}
Application::~Application()
{
    SafeRelease(&m_pDirect2dFactory);
    SafeRelease(&m_pD2dWriteFactory);
    SafeRelease(&MainMenuMainButtonsTextFormat);
    SafeRelease(&m_pNotificationTextFormat);
    SafeRelease(&m_pRenderTarget);
    SafeRelease(&m_pLightSlateGrayBrush);
    SafeRelease(&m_pCornflowerBlueBrush);
    SafeRelease(&m_pBgGreyBrush);
}

void Application::RunMessageLoop() {
    RECT windowSize;
    GetWindowRect(m_hwnd, &windowSize);
    HMONITOR screenHWND = MonitorFromWindow(m_hwnd, MONITOR_DEFAULTTOPRIMARY);
    MONITORINFO screenInfo;
    screenInfo.cbSize = sizeof(MONITORINFO);
    GetMonitorInfoW(screenHWND, &screenInfo);
    windowSize = screenInfo.rcMonitor;
    D2D1_SIZE_U dim = { (windowSize.right - windowSize.left), (windowSize.bottom - windowSize.top) };
    this->ResizeWindow(dim.width, dim.height);
    MoveWindow(m_hwnd, 0, 0, dim.width, dim.height, false);
    dim = { 1920, 1080 };
    this->m_pRenderTarget->Resize(dim);
    this->m_pRenderTarget->BeginDraw();
    this->m_pRenderTarget->Clear(D2D1::ColorF(0x000000));
    this->m_pRenderTarget->EndDraw();
    SetWindowPos(this->m_hwnd, HWND_TOP, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), SWP_FRAMECHANGED);

    MSG msg;
    POINT p;

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
    //* * * * * * * * * * * * * * * * WINDOW MENU BAR * * * * * * * * * * * * * * * * * //
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    POINT position, size;

    UIcollection windowHandler(m_hwnd, m_pRenderTarget, 0);
    position.x = 0;
    position.y = 0;
    UIelement exitButton(position, 0x10);
    exitButton.reStyle(2);
    exitButton.opacityFactor1 = 0;
    UIelement MoveSection(position, 0);
    size.x = 3000;
    size.y = 27;
    MoveSection.resize(size);
    MoveSection.reStyle(0);
    POINT moveWindowOrigin;
    POINT moveWindowNow;
    bool _isWindowMoving = false;
    windowHandler._doDrawBg(false);
    bool _isDrawing = true;
    POINT originClick;
    originClick.x = -1000;
    originClick.y = -1000;
    POINT endClick;
    endClick.x = -1000;
    endClick.y = -1000;
    windowHandler.insertElement(exitButton, 99);
    this->windowHandle = &windowHandler;

    bool endTalk = false;

    while (!endTalk) {
        PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
        if (GetCursorPos(&p)) {
            if (ScreenToClient(m_hwnd, &p)) {

                // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
                //* * * * * * * * * * * * * * * * WINDOW MENU BAR * * * * * * * * * * * * * * * * * //
                // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

                if (_isWindowMoving) {
                    RECT windowSize;
                    if (GetWindowRect(m_hwnd, &windowSize)) {
                        moveWindowNow = p;
                        MoveWindow(
                            m_hwnd,
                            windowSize.left + (moveWindowNow.x - moveWindowOrigin.x),
                            windowSize.top + (moveWindowNow.y - moveWindowOrigin.y),
                            windowSize.right - windowSize.left,
                            windowSize.bottom - windowSize.top,
                            1
                        );

                    }
                }
                if (this->windowHandle->isDisplayed()) {
                    bool _isExitButtonHovered = false;
                    if (this->windowHandle->checkIfHovered(p)) {
                        for (int i = 0; !(this->windowHandle->_isHovered[i] < 0); i++) {
                            if (this->windowHandle->_isHovered[i] == 99) _isExitButtonHovered = true;
                        }
                        if (_isExitButtonHovered) {
                            UIelement* pOpacity = windowHandle->getElementPointer(99);
                            float* opacity = &pOpacity->opacityFactor1;
                            *opacity = *opacity + 0.2f;
                            if (*opacity > 1) *opacity = 1;
                        }
                    }
                    if (!_isExitButtonHovered) {
                        UIelement* pOpacity = windowHandle->getElementPointer(99);
                        float* opacity = &pOpacity->opacityFactor1;
                        *opacity = *opacity - 0.2f;
                        if (*opacity < 0) *opacity = 0;
                    }
                }

                //Check mouse button
                if (msg.message == WM_LBUTTONDOWN) {
                    if (this->windowHandle->isDisplayed()) {
                        if (this->windowHandle->checkIfHovered(p)) {
                            bool _isButton0Hovered = false;
                            bool _isExitButtonHovered = false;
                            for (int i = 0; !(this->windowHandle->_isHovered[i] < 0); i++) {
                                if (this->windowHandle->_isHovered[i] == 0) _isButton0Hovered = true;
                                if (this->windowHandle->_isHovered[i] == 99) _isExitButtonHovered = true;
                            }
                            if (_isButton0Hovered && !_isExitButtonHovered) {
                                _isWindowMoving = true;
                                moveWindowOrigin = p;
                            }
                        }
                    }
                }

                if (_isDrawing) endClick = p;
                if (msg.message == WM_LBUTTONUP) {
                    if (this->windowHandle->isDisplayed()) {
                        if (this->windowHandle->checkIfHovered(p)) {
                            bool _isButton0Hovered = false;
                            bool _isExitButtonHovered = false;
                            bool _isPopupMenuHovered = false;
                            for (int i = 0; !(this->windowHandle->_isHovered[i] < 0); i++) {
                                if (this->windowHandle->_isHovered[i] == 0) _isButton0Hovered = true;
                                if (this->windowHandle->_isHovered[i] == 99) _isExitButtonHovered = true;
                            }
                            if (_isExitButtonHovered) {
                                exit(0);
                            }
                            if (_isButton0Hovered) {
                                _isWindowMoving = false;
                            }
                        }
                    }
                    _isDrawing = false;
                }
            }
        }

        this->m_pRenderTarget->BeginDraw();
        this->m_pRenderTarget->Clear(D2D1::ColorF(0x000000));
        this->actualizeWindow(this->windowHandle);
        this->m_pRenderTarget->EndDraw();
    }
}
void Application::actualizeWindow(UIcollection* activeMenu) {
    /*
    m_pRenderTarget->Clear(D2D1::ColorF(0x00FF00));
    m_pRenderTarget->EndDraw();
    Sleep(2000);
    m_pRenderTarget->BeginDraw();
    */
    activeMenu->drawElements(this->m_hwnd, this->m_pRenderTarget);

}

HRESULT Application::Initialize(string appName, string windowName, int sizeX, int sizeY) {
    HRESULT hr;

    std::wstring txt = std::wstring(appName.begin(), appName.end());
    const wchar_t* w_appName;
    w_appName = txt.c_str();


    txt = std::wstring(windowName.begin(), windowName.end());
    const wchar_t* w_windowName;
    w_windowName = txt.c_str();

    // Initialize device-indpendent resources, such
    // as the Direct2D factory.
    hr = CreateDeviceIndependentResources();

    if (SUCCEEDED(hr))
    {
        // Register the window class.
        WNDCLASSEX wcex = { sizeof(WNDCLASSEX) };
        wcex.style = CS_HREDRAW | CS_VREDRAW;
        wcex.lpfnWndProc = Application::WndProc;
        wcex.cbClsExtra = 0;
        wcex.cbWndExtra = sizeof(LONG_PTR);
        wcex.hInstance = HINST_THISCOMPONENT;
        wcex.hbrBackground = NULL;
        wcex.lpszMenuName = NULL;
        wcex.hCursor = LoadCursor(NULL, IDI_APPLICATION);
        wcex.lpszClassName = w_appName;

        RegisterClassEx(&wcex);


        //// Because the CreateWindow function takes its size in pixels,
        //// obtain the system DPI and use it to scale the window size.
        //FLOAT dpiX, dpiY;

        //// The factory returns the current system DPI. This is also the value it will use
        //// to create its own windows.
        ////GetDpiForWindow(this->m_hwnd);


        // Create the window.
        m_hwnd = CreateWindow(
            w_appName,
            w_windowName,
            WS_VISIBLE,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            sizeX,
            sizeY,
            NULL,
            NULL,
            HINST_THISCOMPONENT,
            this
        );

        this->windowStyle = WS_EX_LAYERED;

        // Hide UIcollection
        SetWindowLong(m_hwnd, GWL_STYLE, 0); //remove all window styles, check MSDN for details

        ShowWindow(m_hwnd, SW_HIDE);
        ::SetMenu(m_hwnd, NULL);


        hr = m_hwnd ? S_OK : E_FAIL;
        if (SUCCEEDED(hr))
        {
            ShowWindow(m_hwnd, SW_SHOWNORMAL);
            UpdateWindow(m_hwnd); // <-
        }
        RECT rc;
        rc.right = sizeX;
        rc.bottom = sizeY;
        DWORD style = WS_POPUP | WS_CLIPCHILDREN;
        DWORD exstyle = 0;
        //AdjustWindowRectEx(&rc, style, false, exstyle);
        this->ResizeAll(InitialWindowSizeX, InitialWindowSizeY);
        //hm
    }

    return hr;
}
HRESULT Application::InitializeLayered(string appName, string windowName, int sizeX, int sizeY) {
    HRESULT hr;

    std::wstring txt = std::wstring(appName.begin(), appName.end());
    const wchar_t* w_appName;
    w_appName = txt.c_str();


    txt = std::wstring(windowName.begin(), windowName.end());
    const wchar_t* w_windowName;
    w_windowName = txt.c_str();

    // Initialize device-indpendent resources, such
    // as the Direct2D factory.
    hr = CreateDeviceIndependentResources();

    if (SUCCEEDED(hr))
    {
        // Register the window class.
        WNDCLASSEX wcex = { sizeof(WNDCLASSEX) };
        wcex.style = CS_HREDRAW | CS_VREDRAW;
        wcex.lpfnWndProc = Application::WndProc;
        wcex.cbClsExtra = 0;
        wcex.cbWndExtra = sizeof(LONG_PTR);
        wcex.hInstance = HINST_THISCOMPONENT;
        wcex.hbrBackground = NULL;
        wcex.lpszMenuName = NULL;
        wcex.hCursor = LoadCursor(NULL, IDI_APPLICATION);
        wcex.lpszClassName = w_appName;

        RegisterClassEx(&wcex);


        //// Because the CreateWindow function takes its size in pixels,
        //// obtain the system DPI and use it to scale the window size.
        //FLOAT dpiX, dpiY;

        //// The factory returns the current system DPI. This is also the value it will use
        //// to create its own windows.
        ////GetDpiForWindow(this->m_hwnd);


        // Create the window.
        m_hwnd = CreateWindowExW(
            WS_EX_LAYERED,
            w_appName,
            w_windowName,
            WS_VISIBLE,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            sizeX,
            sizeY,
            NULL,
            NULL,
            HINST_THISCOMPONENT,
            this
        );

        this->windowStyle = WS_EX_LAYERED;

        // Hide UIcollection
        SetWindowLong(m_hwnd, GWL_STYLE, 0); //remove all window styles, check MSDN for details

        ShowWindow(m_hwnd, SW_HIDE);
        ::SetMenu(m_hwnd, NULL);


        hr = m_hwnd ? S_OK : E_FAIL;
        if (SUCCEEDED(hr))
        {
            ShowWindow(m_hwnd, SW_SHOWNORMAL);
            UpdateWindow(m_hwnd);
        }
        RECT rc;
        rc.right = sizeX;
        rc.bottom = sizeY;
        DWORD style = WS_POPUP | WS_CLIPCHILDREN;
        DWORD exstyle = 0;
        //AdjustWindowRectEx(&rc, style, false, exstyle);
    }

    return hr;
}


HRESULT Application::CreateDeviceIndependentResources() {
    HRESULT hr = S_OK;

    // Create a Direct2D factory.
    hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pDirect2dFactory);

    return hr;
}
HRESULT Application::CreateDeviceResources() {
    HRESULT hr = S_OK;

    if (!m_pRenderTarget)
    {
        RECT rc;
        GetClientRect(m_hwnd, &rc);

        D2D1_SIZE_U size = D2D1::SizeU(
            rc.right - rc.left,
            rc.bottom - rc.top
        );

        // Create a Direct2D render target.
        hr = m_pDirect2dFactory->CreateHwndRenderTarget(
            D2D1::RenderTargetProperties(),
            D2D1::HwndRenderTargetProperties(m_hwnd, size),
            &m_pRenderTarget
        );


        if (SUCCEEDED(hr))
        {
            // Create a gray brush.
            hr = m_pRenderTarget->CreateSolidColorBrush(
                D2D1::ColorF(D2D1::ColorF::LightSlateGray),
                &m_pLightSlateGrayBrush
            );
        }
        if (SUCCEEDED(hr))
        {
            // Create a blue brush.
            hr = m_pRenderTarget->CreateSolidColorBrush(
                D2D1::ColorF(D2D1::ColorF::CornflowerBlue),
                &m_pCornflowerBlueBrush
            );
        }
        if (SUCCEEDED(hr))
        {
            // Create a blue brush.
            hr = m_pRenderTarget->CreateSolidColorBrush(
                D2D1::ColorF(D2D1::ColorF(0x222222)),
                &m_pBgGreyBrush
            );
        }
    }

    return hr;
}

HRESULT Application::CreateDeviceResourcesLayered() {
    HRESULT hr = S_OK;

    if (!m_pRenderTarget)
    {
        RECT rc;
        GetClientRect(m_hwnd, &rc);

        D2D1_SIZE_U size = D2D1::SizeU(
            rc.right - rc.left,
            rc.bottom - rc.top
        );

        // Create a Direct2D render target.
        hr = m_pDirect2dFactory->CreateHwndRenderTarget(
            D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED)),
            D2D1::HwndRenderTargetProperties(m_hwnd, size),
            &m_pRenderTarget
        );


        if (SUCCEEDED(hr))
        {
            // Create a gray brush.
            hr = m_pRenderTarget->CreateSolidColorBrush(
                D2D1::ColorF(D2D1::ColorF::LightSlateGray),
                &m_pLightSlateGrayBrush
            );
        }
        if (SUCCEEDED(hr))
        {
            // Create a blue brush.
            hr = m_pRenderTarget->CreateSolidColorBrush(
                D2D1::ColorF(D2D1::ColorF::CornflowerBlue),
                &m_pCornflowerBlueBrush
            );
        }
        if (SUCCEEDED(hr))
        {
            // Create a blue brush.
            hr = m_pRenderTarget->CreateSolidColorBrush(
                D2D1::ColorF(D2D1::ColorF(0x222222)),
                &m_pBgGreyBrush
            );
        }
    }

    return hr;
}

void Application::DiscardDeviceResources()
{
    SafeRelease(&m_pRenderTarget);
    SafeRelease(&m_pLightSlateGrayBrush);
    SafeRelease(&m_pCornflowerBlueBrush);
}
void Application::DiscardDeviceResourcesLayered()
{
    SafeRelease(&m_pRenderTarget);
    SafeRelease(&m_pLightSlateGrayBrush);
    SafeRelease(&m_pCornflowerBlueBrush);
}
void Application::resizeRenderTargetToWindowSize() {
    RECT rc;
    GetClientRect(m_hwnd, &rc);

    D2D1_SIZE_U size = D2D1::SizeU(rc.right, rc.bottom);

    m_pRenderTarget->Resize(size);
    InvalidateRect(m_hwnd, NULL, FALSE);
}
LRESULT CALLBACK Application::WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;

    if (message == WM_CREATE)
    {
        LPCREATESTRUCT pcs = (LPCREATESTRUCT)lParam;
        Application* pDemoApp = (Application*)pcs->lpCreateParams;

        ::SetWindowLongPtrW(
            hwnd,
            GWLP_USERDATA,
            reinterpret_cast<LONG_PTR>(pDemoApp)
        );

        result = 1;
    }
    else
    {
        Application* pDemoApp = reinterpret_cast<Application*>(static_cast<LONG_PTR>(
            ::GetWindowLongPtrW(
                hwnd,
                GWLP_USERDATA
            )));

        bool wasHandled = false;

        if (pDemoApp)
        {
            switch (message)
            {
            case WM_SIZE:
            {

                UINT width = LOWORD(lParam);
                UINT height = HIWORD(lParam);
                pDemoApp->OnResize(width, height);
            }
            result = 0;
            wasHandled = true;
            break;

            case WM_DISPLAYCHANGE:
            {
                InvalidateRect(hwnd, NULL, FALSE);
            }
            result = 0;
            wasHandled = true;
            break;

            case WM_PAINT:
            {
                pDemoApp->OnRender();
                ValidateRect(hwnd, NULL);
            }
            result = 0;
            wasHandled = true;
            break;

            case WM_DESTROY:
            {
                PostQuitMessage(0);
            }
            result = 1;
            wasHandled = true;
            break;
            }
        }

        if (!wasHandled)
        {
            result = DefWindowProc(hwnd, message, wParam, lParam);
        }
    }

    return result;
}
HRESULT Application::OnRender()
{
    HRESULT hr = S_OK;

    hr = CreateDeviceResources();
    if (SUCCEEDED(hr)) {

        m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
    }
    if (hr == D2DERR_RECREATE_TARGET)
    {
        hr = S_OK;
        DiscardDeviceResources();
    }
    return hr;
}
void Application::ResizeWindow(int x, int y) {
    RECT rc;
    GetClientRect(m_hwnd, &rc);
    SetWindowPos(this->m_hwnd, HWND_TOP, rc.left, rc.top, rc.left + x, rc.top + y, SWP_SHOWWINDOW);
    D2D1_SIZE_U size = D2D1::SizeU(x, y);
}

void Application::ResizeAll(int x, int y) {
    RECT rc;
    GetClientRect(m_hwnd, &rc);
    SetWindowPos(this->m_hwnd, HWND_TOP, rc.left, rc.top, rc.left + x, rc.top + y, SWP_SHOWWINDOW);
    D2D1_SIZE_U size = D2D1::SizeU(x, y);
    this->m_pRenderTarget->Resize(size);
    InvalidateRect(m_hwnd, NULL, FALSE);
}

// A quoi cette fonction sert actuellement...? La question... Demeure.
void Application::OnResize(UINT width, UINT height)
{
    if (m_pRenderTarget)
    {
        // Note: This method can fail, but it's okay to ignore the
        // error here, because the error will be returned again
        // the next time EndDraw is called.
        m_pRenderTarget->BeginDraw();
        //resizeRenderTargetToWindowSize();
        m_pRenderTarget->Resize(D2D1::SizeU(width, height));
        m_pRenderTarget->EndDraw();
    }
}

void Application::hide() {
    ShowWindow(this->m_hwnd, SW_HIDE);
}

void Application::show() {
    ShowWindow(this->m_hwnd, SW_SHOW);
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * GAME ELEMENTS * * * * * * * * * * * * * * * * *//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

void Application::writeTextOnScreenLoopWithWindowHandle(std::string text, int time) {

    this->ResizeWindow(1366, 768);
    D2D1_SIZE_U dim = { 1920, 1080 };
    this->m_pRenderTarget->Resize(dim);

    MSG msg;
    POINT p;

    POINT position, size;
    UIcollection windowHandler(this->m_hwnd, this->m_pRenderTarget, 0);
    position.x = 0;
    position.y = 0;
    UIelement exitButton(position, 0x10);
    exitButton.reStyle(2);
    exitButton.opacityFactor1 = 0;
    UIelement MoveSection(position, 0);
    size.x = 3000;
    size.y = 27;
    MoveSection.resize(size);
    MoveSection.reStyle(0);
    POINT moveWindowOrigin;
    POINT moveWindowNow;
    bool _isWindowMoving = false;
    windowHandler._doDrawBg(false);
    bool _isDrawing = true;
    POINT originClick;
    originClick.x = -1000;
    originClick.y = -1000;
    POINT endClick;
    endClick.x = -1000;
    endClick.y = -1000;


    windowHandler.addElement(MoveSection);
    windowHandler.insertElement(exitButton, 99);
    this->windowHandle = &windowHandler;

    int elapsedMillis = 0;
    std::chrono::time_point start = std::chrono::system_clock::now();

    while (elapsedMillis < time) {
        this->m_pRenderTarget->BeginDraw();
        this->m_pRenderTarget->Clear(D2D1::ColorF(0x000000));
        PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
        TranslateMessage(&msg);

        RECT rc;
        GetClientRect(m_hwnd, &rc);

        if (GetCursorPos(&p)) {
            if (ScreenToClient(m_hwnd, &p)) {

                //p.x = p.x * ((float)m_pRenderTarget->GetSize().width / (float)(rc.right - rc.left));
                //p.y = p.y * ((float)m_pRenderTarget->GetSize().height / (float)(rc.bottom - rc.top));

                // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
                //* * * * * * * * * * * * * * * * WINDOW MENU BAR * * * * * * * * * * * * * * * * * //
                // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

                if (_isWindowMoving) {
                    RECT windowSize;
                    if (GetWindowRect(m_hwnd, &windowSize)) {
                        moveWindowNow = p;
                        MoveWindow(
                            m_hwnd,
                            windowSize.left + (moveWindowNow.x - moveWindowOrigin.x),
                            windowSize.top + (moveWindowNow.y - moveWindowOrigin.y),
                            windowSize.right - windowSize.left,
                            windowSize.bottom - windowSize.top,
                            1
                        );

                    }
                }
                if (this->windowHandle->isDisplayed()) {
                    bool _isExitButtonHovered = false;
                    if (this->windowHandle->checkIfHovered(p)) {
                        for (int i = 0; !(this->windowHandle->_isHovered[i] < 0); i++) {
                            if (this->windowHandle->_isHovered[i] == 99) _isExitButtonHovered = true;
                        }
                        if (_isExitButtonHovered) {
                            UIelement* pOpacity = windowHandle->getElementPointer(99);
                            float* opacity = &pOpacity->opacityFactor1;
                            *opacity = *opacity + 0.2f;
                            if (*opacity > 1) *opacity = 1;
                        }
                    }
                    if (!_isExitButtonHovered) {
                        UIelement* pOpacity = windowHandle->getElementPointer(99);
                        float* opacity = &pOpacity->opacityFactor1;
                        *opacity = *opacity - 0.2f;
                        if (*opacity < 0) *opacity = 0;
                    }
                }

                //Check mouse button
                if (msg.message == WM_LBUTTONDOWN) {
                    if (this->windowHandle->isDisplayed()) {
                        if (this->windowHandle->checkIfHovered(p)) {
                            bool _isButton0Hovered = false;
                            bool _isExitButtonHovered = false;
                            for (int i = 0; !(this->windowHandle->_isHovered[i] < 0); i++) {
                                if (this->windowHandle->_isHovered[i] == 0) _isButton0Hovered = true;
                                if (this->windowHandle->_isHovered[i] == 99) _isExitButtonHovered = true;
                            }
                            if (_isButton0Hovered && !_isExitButtonHovered) {
                                _isWindowMoving = true;
                                moveWindowOrigin = p;
                            }
                        }
                    }
                }

                if (_isDrawing) endClick = p;
                if (msg.message == WM_LBUTTONUP) {
                    if (this->windowHandle->isDisplayed()) {
                        if (this->windowHandle->checkIfHovered(p)) {
                            bool _isButton0Hovered = false;
                            bool _isExitButtonHovered = false;
                            bool _isPopupMenuHovered = false;
                            for (int i = 0; !(this->windowHandle->_isHovered[i] < 0); i++) {
                                if (this->windowHandle->_isHovered[i] == 0) _isButton0Hovered = true;
                                if (this->windowHandle->_isHovered[i] == 99) _isExitButtonHovered = true;
                            }
                            if (_isExitButtonHovered) {
                                exit(0);
                            }
                            if (_isButton0Hovered) {
                                _isWindowMoving = false;
                            }
                        }
                    }
                    _isDrawing = false;
                }
            }
        }
        this->writeTextOnScreen(text);
        this->actualizeWindow(this->windowHandle);
        this->m_pRenderTarget->EndDraw();

        std::chrono::time_point now = std::chrono::system_clock::now();
        std::chrono::time_point startCompar = start;
        elapsedMillis = 0;
        while (now >= startCompar) {
            elapsedMillis++;
            startCompar = startCompar + std::chrono::milliseconds(1);
        }
    }
}
bool Application::writeTextOnScreen(std::string text) {
    IDWriteTextFormat* textFormat = nullptr;
    IDWriteFactory* m_pD2dWriteFactoryMeh;

    HRESULT hr;
    hr = DWriteCreateFactory(
        DWRITE_FACTORY_TYPE_SHARED,
        __uuidof(IDWriteFactory),
        reinterpret_cast<IUnknown**>(&m_pD2dWriteFactoryMeh)
    );

    ID2D1SolidColorBrush* m_pColorBrush = nullptr;

    if (SUCCEEDED(hr)) {
        hr = this->m_pRenderTarget->CreateSolidColorBrush(
            D2D1::ColorF(0xFFFFFF),
            &m_pColorBrush
        );
    }
    int i = 0;
    std::wstring txt = std::wstring(text.begin(), text.end());
    const wchar_t* sc_text;
    sc_text = txt.c_str();
    for (i; sc_text[i] != NULL; i++);
    if (SUCCEEDED(hr)) {
        hr = m_pD2dWriteFactoryMeh->CreateTextFormat(
            L"Gabriola",
            NULL,
            DWRITE_FONT_WEIGHT_NORMAL,
            DWRITE_FONT_STYLE_NORMAL,
            DWRITE_FONT_STRETCH_NORMAL,
            100.f,
            L"", //locale
            &textFormat
        );
    }
    RECT window;
    D2D1_SIZE_F a = this->m_pRenderTarget->GetSize();
    D2D1_RECT_F area = D2D1::RectF(
        500,
        500,
        a.width - 500,
        a.height - 500
    );

    if (SUCCEEDED(hr)) {
        this->m_pRenderTarget->DrawText(
            sc_text,
            i,
            textFormat,
            area,
            m_pColorBrush
        );
    }
    SafeRelease(&m_pD2dWriteFactory);
    SafeRelease(&textFormat);
    SafeRelease(&m_pColorBrush);

    if (SUCCEEDED(hr)) {
        return true;
    }
    return false;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * GAME PARTS * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * TESTS * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

void Application::dungeonGenerationTest1() {
    RECT windowSize;
    GetWindowRect(m_hwnd, &windowSize);
    HMONITOR screenHWND = MonitorFromWindow(m_hwnd, MONITOR_DEFAULTTOPRIMARY);
    MONITORINFO screenInfo;
    screenInfo.cbSize = sizeof(MONITORINFO);
    GetMonitorInfoW(screenHWND, &screenInfo);
    windowSize = screenInfo.rcMonitor;
    D2D1_SIZE_U dim = { (windowSize.right - windowSize.left), (windowSize.bottom - windowSize.top) };
    this->ResizeWindow(dim.width, dim.height);
    MoveWindow(m_hwnd, 0, 0, dim.width, dim.height, false);
    dim = { 1920, 1080 };
    this->m_pRenderTarget->Resize(dim);
    this->m_pRenderTarget->BeginDraw();
    this->m_pRenderTarget->Clear(D2D1::ColorF(0x000000));
    this->m_pRenderTarget->EndDraw();
    SetWindowPos(this->m_hwnd, HWND_TOP, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), SWP_FRAMECHANGED);

    MSG msg;
    POINT p;

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
    //* * * * * * * * * * * * * * * * WINDOW MENU BAR * * * * * * * * * * * * * * * * * //
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    POINT position, size;

    UIcollection windowHandler(m_hwnd, m_pRenderTarget, 0);
    position.x = 0;
    position.y = 0;
    UIelement exitButton(position, 0x10);
    exitButton.reStyle(2);
    exitButton.opacityFactor1 = 0;
    UIelement MoveSection(position, 0);
    size.x = 3000;
    size.y = 27;
    MoveSection.resize(size);
    MoveSection.reStyle(0);
    POINT moveWindowOrigin;
    POINT moveWindowNow;
    bool _isWindowMoving = false;
    windowHandler._doDrawBg(false);
    bool _isDrawing = true;
    POINT originClick;
    originClick.x = -1000;
    originClick.y = -1000;
    POINT endClick;
    endClick.x = -1000;
    endClick.y = -1000;
    windowHandler.insertElement(exitButton, 99);
    this->windowHandle = &windowHandler;
    
    bool endTalk = false;

    // TESTING ELEMENTS //
    Agnosia::Chunk test;
    test.generateRandomV1(10, 10, 0, 5, 10);
    test.outputDungeon();

    Jinko::point point(0, 0);
    Agnosia::Map world;
    world.addChunk(&test, point);
    Agnosia::Mob player;
    Jinko::point locationP(4, 6);
    world.addMob(&player);

    Agnosia::inputBoard keyboard;

    UIcollection DungeonDisplayer(m_hwnd, m_pRenderTarget, 0);
    position.x = 0;
    position.y = 0;
    UIelement dungeonText(position, 0);
    dungeonText.reType(0);
    dungeonText.reStyle(5);
    position.x = 1920;
    position.y = 1080;
    dungeonText.resize(position);
    dungeonText.fvalue1 = 40;
    dungeonText.colorValue = 0xFFFFFF;
    DungeonDisplayer.insertElement(dungeonText, 1);

    player.init(std::chrono::system_clock::now());
    while (!endTalk) {
        PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
        if (GetCursorPos(&p)) {
            if (ScreenToClient(m_hwnd, &p)) {
                std::chrono::system_clock::time_point thisCycleTime = std::chrono::system_clock::now();
                // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
                // * * * * * * * * * * * * * * * * WINDOW MENU BAR * * * * * * * * * * * * * * * * * //
                // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

                if (_isWindowMoving) {
                    RECT windowSize;
                    if (GetWindowRect(m_hwnd, &windowSize)) {
                        moveWindowNow = p;
                        MoveWindow(
                            m_hwnd,
                            windowSize.left + (moveWindowNow.x - moveWindowOrigin.x),
                            windowSize.top + (moveWindowNow.y - moveWindowOrigin.y),
                            windowSize.right - windowSize.left,
                            windowSize.bottom - windowSize.top,
                            1
                        );

                    }
                }
                if (this->windowHandle->isDisplayed()) {
                    bool _isExitButtonHovered = false;
                    if (this->windowHandle->checkIfHovered(p)) {
                        for (int i = 0; !(this->windowHandle->_isHovered[i] < 0); i++) {
                            if (this->windowHandle->_isHovered[i] == 99) _isExitButtonHovered = true;
                        }
                        if (_isExitButtonHovered) {
                            UIelement* pOpacity = windowHandle->getElementPointer(99);
                            float* opacity = &pOpacity->opacityFactor1;
                            *opacity = *opacity + 0.2f;
                            if (*opacity > 1) *opacity = 1;
                        }
                    }
                    if (!_isExitButtonHovered) {
                        UIelement* pOpacity = windowHandle->getElementPointer(99);
                        float* opacity = &pOpacity->opacityFactor1;
                        *opacity = *opacity - 0.2f;
                        if (*opacity < 0) *opacity = 0;
                    }
                }

                //Check mouse button
                if (msg.message == WM_LBUTTONDOWN) {
                    if (this->windowHandle->isDisplayed()) {
                        if (this->windowHandle->checkIfHovered(p)) {
                            bool _isButton0Hovered = false;
                            bool _isExitButtonHovered = false;
                            for (int i = 0; !(this->windowHandle->_isHovered[i] < 0); i++) {
                                if (this->windowHandle->_isHovered[i] == 0) _isButton0Hovered = true;
                                if (this->windowHandle->_isHovered[i] == 99) _isExitButtonHovered = true;
                            }
                            if (_isButton0Hovered && !_isExitButtonHovered) {
                                _isWindowMoving = true;
                                moveWindowOrigin = p;
                            }
                        }
                    }
                }

                if (_isDrawing) endClick = p;
                if (msg.message == WM_LBUTTONUP) {
                    if (this->windowHandle->isDisplayed()) {
                        if (this->windowHandle->checkIfHovered(p)) {
                            bool _isButton0Hovered = false;
                            bool _isExitButtonHovered = false;
                            bool _isPopupMenuHovered = false;
                            for (int i = 0; !(this->windowHandle->_isHovered[i] < 0); i++) {
                                if (this->windowHandle->_isHovered[i] == 0) _isButton0Hovered = true;
                                if (this->windowHandle->_isHovered[i] == 99) _isExitButtonHovered = true;
                            }
                            if (_isExitButtonHovered) {
                                exit(0);
                            }
                            if (_isButton0Hovered) {
                                _isWindowMoving = false;
                            }
                        }
                    }
                    _isDrawing = false;
                }

                // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
                // * * * * * * * * * * * * * * * * Dungeon Display * * * * * * * * * * * * * * * * * //
                // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
                keyboard.update();
                Jinko::vector PlayerMovement(0, 0);
                if (keyboard.getKeyboardInput(0) > 1) PlayerMovement.x = -1;
                if (keyboard.getKeyboardInput(1) > 1) PlayerMovement.y = -1;
                if (keyboard.getKeyboardInput(2) > 1) PlayerMovement.x = 1;
                if (keyboard.getKeyboardInput(3) > 1) PlayerMovement.y = 1;
                if ((PlayerMovement.x != 0) || (PlayerMovement.y != 0)) {
                    player.move(thisCycleTime, PlayerMovement, &world);
                }

                test.generateDunStr();
                std::string log = test.mapString + "Player Pos : x = " + to_string(round(player.position.x)) + "y = " + to_string(round(player.position.y));
                DungeonDisplayer.getElementPointer(1)->reText(log);
                world.updateMobs(thisCycleTime);
            }
        }

        this->m_pRenderTarget->BeginDraw();
        this->m_pRenderTarget->Clear(D2D1::ColorF(0x000000));
        this->actualizeWindow(this->windowHandle);
        DungeonDisplayer.drawElements(this->m_hwnd, this->m_pRenderTarget);
        this->m_pRenderTarget->EndDraw();
    }
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * UIELEMENT * * * * * * * * * * * * * * * * * *//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


std::string UIelement::getText() {
    return this->text;
}
unsigned int UIelement::getStyle() {
    return this->style;
}
bool UIelement::get_isVoid() {
    return this->_isVoid;
}
unsigned int UIelement::getAlignement() {
    return this->alignement;
}

void UIelement::reText(std::string text) {
    this->text = text;
}
void UIelement::reStyle(unsigned int style) {
    this->style = style;
}
void UIelement::reType(unsigned int type) {
    this->type = type;
}
void UIelement::re_isVoid(bool _isVoid) {
    this->_isVoid = _isVoid;
}

bool UIelement::updatePosition(HWND m_hwnd) {
    POINT* p;
    p = &this->currentPosition;
    RECT size;
    if (GetWindowRect(m_hwnd, &size)) {
        p->x = size.right - size.left;
        p->y = size.bottom - size.top;
        if (ScreenToClient(m_hwnd, p)) {
            return true;
        }
        else return false;
    }
    else return false;
}
void UIelement::setImagePath(string adress) {
    this->adress = adress;
}
std::string UIelement::getImagePath() {
    return this->adress;
}


// POINT p is used as an offset. Used mainly for image displaying. For static buttons, prefer using the POINT position in UIelement.
bool UIelement::drawElement(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    bool test;
    if (this->updatePosition(m_hwnd)) {
        switch (this->type) {
        case 0:
            switch (this->style) {
            case 0:
                this->generateRectangleCollisionSection();
                return true;
                break;
            case 1:
                return this->drawBasicButton(m_hwnd, m_pRenderTarget);
                break;
            case 2:
                return this->drawExitButton(m_hwnd, m_pRenderTarget);
                break;
            case 3:
                return this->drawPopupTextWindow(m_hwnd, m_pRenderTarget);
                break;
            case 4:
                return this->drawPopupCloseButton(m_hwnd, m_pRenderTarget);
                break;
            case 5:
                return this->drawText(m_hwnd, m_pRenderTarget);
                break;
            case 6:
                return this->drawTextProgressively(m_hwnd, m_pRenderTarget);
                break;
            case 7:
                return this->drawPixelizedBlackFrame(m_hwnd, m_pRenderTarget);
                break;
            default:
                return false;
            }
            break;
        case 1:
            switch (this->style) {
            case 0:
                return this->drawImage(m_hwnd, m_pRenderTarget);
                break;
            case 1:
                return this->drawImageFromHandle(m_hwnd, m_pRenderTarget);
                break;
            default:
                return false;
            }
        case 2:
            return this->drawAnimation(m_hwnd, m_pRenderTarget, this->style);
            break;
        case 3:
            switch (this->style) {
            default:
                return false;
            }
        case 4:
            switch (this->style) {
            case 0:
                return this->renderTargetTransformationReset(m_hwnd, m_pRenderTarget);
                break;
            case 1:
                return this->renderTargetTransformationCylinderEffect(m_hwnd, m_pRenderTarget);
                break;
            default:
                return false;
            }
        }
    }
    else return false;
    return false;
}

void UIelement::generateRectangleCollisionSection() {
    RECT area;
    area.left = this->position.x;
    area.top = this->position.y;
    area.right = this->position.x + this->size.x;
    area.bottom = this->position.y + this->size.y;
    this->activeArea = area;
}
void UIelement::generateRectangleCollisionSection(int x1, int y1, int x2, int y2) {
    RECT area;
    area.left = x1;
    area.top = y1;
    area.right = x2;
    area.bottom = y2;
    this->activeArea = area;
}


bool UIelement::drawBasicButton(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    std::wstring txt = std::wstring(this->text.begin(), this->text.end());
    const wchar_t* sc_text;
    sc_text = txt.c_str();

    RECT windowSize;
    if (GetWindowRect(m_hwnd, &windowSize)) {
        D2D1_SIZE_F rtSize = m_pRenderTarget->GetSize();
        int width = static_cast<int>(rtSize.width);
        int height = static_cast<int>(rtSize.height);
        HRESULT hr;
        ID2D1SolidColorBrush* m_pWhiteBrush;
        hr = m_pRenderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::White),
            &m_pWhiteBrush
        );
        if (SUCCEEDED(hr)) {
            ID2D1SolidColorBrush* m_pForegroundGreyBrush;
            hr = m_pRenderTarget->CreateSolidColorBrush(
                D2D1::ColorF(D2D1::ColorF(0x555555)),
                &m_pForegroundGreyBrush
            );
            if (SUCCEEDED(hr)) {
                POINT center;
                center.x = (windowSize.right - windowSize.left) / 2;
                center.y = (windowSize.bottom - windowSize.top) / 2;
                /*
                center.x = width/2;
                center.y = height/2;
                */
                POINT relativePosition;
                int xFactor = 1;
                int yFactor = 1;
                POINT relativeSize;
                D2D1_RECT_F rectangle1;

                relativePosition.x = ((1000 * (this->position.x * (windowSize.right - windowSize.left))) / InitialWindowSizeX) / 1000;
                relativePosition.y = ((1000 * (this->position.y * (windowSize.bottom - windowSize.top))) / InitialWindowSizeY) / 1000;
                if (this->position.x < 0) xFactor = -xFactor;
                if (this->position.y < 0) yFactor = -yFactor;
                relativeSize.x = ((1000 * (this->size.x * (windowSize.right - windowSize.left))) / InitialWindowSizeX) / 1000;
                relativeSize.y = ((1000 * (this->size.y * (windowSize.bottom - windowSize.top))) / InitialWindowSizeY) / 1000;

                bool aligned = false;

                rectangle1 = D2D1::RectF(0, 0, 0, 0);

                if ((this->alignement & 0x01) == 0x01) {
                    rectangle1.top = relativePosition.y;
                    rectangle1.bottom = relativeSize.y + relativePosition.y;
                    aligned = true;
                }
                else if ((this->alignement & 0x02) == 0x02) {
                    rectangle1.left = windowSize.bottom - (relativePosition.x);
                    rectangle1.right = windowSize.bottom - (relativeSize.x + relativePosition.x);
                    aligned = true;
                }
                if ((this->alignement & 0x04) == 0x04) {
                    rectangle1.left = relativePosition.x;
                    rectangle1.right = relativeSize.x + relativePosition.x;
                    aligned = true;
                }
                else if ((this->alignement & 0x08) == 0x08) {
                    rectangle1.left = windowSize.right - relativeSize.x;
                    rectangle1.right = windowSize.right - (relativeSize.x + relativePosition.x);
                    aligned = true;
                }
                if ((this->alignement & 0x10) == 0x10) {
                    rectangle1 = D2D1::RectF(
                        center.x - (relativeSize.x / 2) + relativePosition.x,
                        center.y - (relativeSize.y / 2) + relativePosition.y,
                        center.x + (relativeSize.x / 2) + relativePosition.x,
                        center.y + (relativeSize.y / 2) + relativePosition.y
                    );
                    aligned = true;
                }


                if (!aligned) { // agit comme (et pour) 0x00.
                    rectangle1 = D2D1::RectF(
                        relativePosition.x,
                        relativePosition.y,
                        relativeSize.x + relativePosition.x,
                        relativeSize.y + relativePosition.y
                    );
                }


                //relativeSize.x = ((10000 * (this->size.x * ((10000 * this->size.x) / InitialWindowSizeX))) / (10000 * InitialWindowSizeX / (displaySize.right - displaySize.left))) / 10000;
                //relativeSize.y = ((10000 * (this->size.y * ((10000 * this->size.y) / InitialWindowSizeY))) / (10000 * InitialWindowSizeY / (displaySize.bottom - displaySize.top))) / 10000;
                //ScreenToClient(m_hwnd, &center);

                this->activeArea.bottom = rectangle1.bottom;
                this->activeArea.top = rectangle1.top;
                this->activeArea.left = rectangle1.left;
                this->activeArea.right = rectangle1.right;
                m_pRenderTarget->FillRectangle(&rectangle1, m_pForegroundGreyBrush);
                m_pRenderTarget->DrawRectangle(&rectangle1, m_pWhiteBrush);

                int i = 0;
                for (i; sc_text[i] != NULL; i++);
                m_pRenderTarget->DrawText(
                    sc_text,
                    i,
                    MainMenuMainButtonsTextFormat,
                    D2D1::RectF(rectangle1.left, rectangle1.top, rectangle1.right, rectangle1.bottom),
                    m_pWhiteBrush
                );
                SafeRelease(&m_pWhiteBrush);
                SafeRelease(&m_pForegroundGreyBrush);
                return true;
            }
            else {
                SafeRelease(&m_pWhiteBrush);
                SafeRelease(&m_pForegroundGreyBrush);
                return false;
            }
        }
        else {
            SafeRelease(&m_pWhiteBrush);
            return false;
        }
    }
    else return false;
}
bool UIelement::drawExitButton(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    D2D1_SIZE_U windowDim = m_pRenderTarget->GetPixelSize();
    RECT displaySize{ 0, 0, windowDim.width, windowDim.height };
    HRESULT hr;
    ID2D1SolidColorBrush* m_pWhiteBrush;
    hr = m_pRenderTarget->CreateSolidColorBrush(
        D2D1::ColorF(D2D1::ColorF(0xFFFFFF)),
        &m_pWhiteBrush
    );
    if (SUCCEEDED(hr)) {
        ID2D1SolidColorBrush* m_pRedBrush;
        hr = m_pRenderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF(0xCC0000)),
            &m_pRedBrush
        );
        m_pRedBrush->SetOpacity(this->opacityFactor1);

        if (SUCCEEDED(hr)) {
            D2D1_RECT_F rectangle1 = D2D1::RectF(
                (displaySize.right - displaySize.left),
                0,
                (displaySize.right - displaySize.left) - 51,
                27
            );
            D2D1_POINT_2F p1 = D2D1::Point2F((displaySize.right - displaySize.left) - 31, 8);
            D2D1_POINT_2F p2 = D2D1::Point2F((displaySize.right - displaySize.left) - 20, 19);
            D2D1_POINT_2F p3 = D2D1::Point2F((displaySize.right - displaySize.left) - 31, 19);
            D2D1_POINT_2F p4 = D2D1::Point2F((displaySize.right - displaySize.left) - 20, 8);
            m_pRenderTarget->FillRectangle(&rectangle1, m_pRedBrush);
            m_pRenderTarget->DrawLine(p1, p2, m_pWhiteBrush);
            m_pRenderTarget->DrawLine(p3, p4, m_pWhiteBrush);

            RECT windowSize;
            if (GetWindowRect(m_hwnd, &windowSize)) {
                if (displaySize.right < 1) {}
                else {
                    rectangle1 = D2D1::RectF(
                        (windowSize.right - windowSize.left),
                        0,
                        (windowSize.right - windowSize.left) - ((51 * windowSize.right - windowSize.left) / (displaySize.right - displaySize.left)),
                        ((27 * windowSize.right - windowSize.left) / (displaySize.right - displaySize.left))
                    );
                    this->activeArea.bottom = rectangle1.bottom;
                    this->activeArea.top = rectangle1.top;
                    this->activeArea.left = rectangle1.left;
                    this->activeArea.right = rectangle1.right;
                }
            }

            SafeRelease(&m_pWhiteBrush);
            SafeRelease(&m_pRedBrush);
            return true;
        }
        else {
            SafeRelease(&m_pRedBrush);
            SafeRelease(&m_pWhiteBrush);
            return false;
        }
    }
    else {
        SafeRelease(&m_pWhiteBrush);
        return false;
    }
    return false;
}
bool UIelement::drawPopupTextWindow(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    RECT windowSize;
    if (GetWindowRect(m_hwnd, &windowSize)) {
        HRESULT hr;
        ID2D1SolidColorBrush* m_pWhiteBrush;
        hr = m_pRenderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF(0xFFFFFF)),
            &m_pWhiteBrush
        );
        if (SUCCEEDED(hr)) {
            ID2D1SolidColorBrush* m_pGreyBrush;
            hr = m_pRenderTarget->CreateSolidColorBrush(
                D2D1::ColorF(D2D1::ColorF(0x333333)),
                &m_pGreyBrush
            );
            if (SUCCEEDED(hr)) {

                D2D1_RECT_F rectangle1 = D2D1::RectF(
                    0,
                    (windowSize.bottom - windowSize.top) - 27,
                    (windowSize.right - windowSize.left),
                    (windowSize.bottom - windowSize.top)
                );
                this->activeArea.bottom = rectangle1.bottom;
                this->activeArea.top = rectangle1.top;
                this->activeArea.left = rectangle1.left;
                this->activeArea.right = rectangle1.right;
                m_pRenderTarget->FillRectangle(&rectangle1, m_pGreyBrush);

                int i = 0;
                std::wstring txt = std::wstring(this->text.begin(), this->text.end());
                const wchar_t* sc_text;
                sc_text = txt.c_str();
                for (i; sc_text[i] != NULL; i++);
                hr = m_pD2dWriteFactory->CreateTextFormat(
                    L"Gabriola",
                    NULL,
                    DWRITE_FONT_WEIGHT_NORMAL,
                    DWRITE_FONT_STYLE_NORMAL,
                    DWRITE_FONT_STRETCH_NORMAL,
                    13.0f,
                    L"", //locale
                    &this->textFormat
                );
                if (SUCCEEDED(hr)) {
                    m_pRenderTarget->DrawText(
                        sc_text,
                        i,
                        textFormat,
                        D2D1::RectF(rectangle1.left + 3, rectangle1.top, rectangle1.right, rectangle1.bottom),
                        m_pWhiteBrush
                    );
                }

                SafeRelease(&m_pWhiteBrush);
                SafeRelease(&m_pGreyBrush);
                SafeRelease(&textFormat);
                return true;
            }
            else {
                SafeRelease(&m_pWhiteBrush);
                SafeRelease(&m_pGreyBrush);
                return false;
            }
        }
        else {
            SafeRelease(&m_pWhiteBrush);
            return false;
        }
    }
    else return false;
}
bool UIelement::drawPopupCloseButton(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    if (1) {
        RECT windowSize;
        if (GetWindowRect(m_hwnd, &windowSize)) {
            HRESULT hr;
            ID2D1SolidColorBrush* m_pWhiteBrush;
            hr = m_pRenderTarget->CreateSolidColorBrush(
                D2D1::ColorF(D2D1::ColorF(0xFFFFFF)),
                &m_pWhiteBrush
            );
            if (SUCCEEDED(hr)) {
                ID2D1SolidColorBrush* m_pDarkGreyBrush;
                hr = m_pRenderTarget->CreateSolidColorBrush(
                    D2D1::ColorF(D2D1::ColorF(0x222222)),
                    &m_pDarkGreyBrush
                );
                m_pDarkGreyBrush->SetOpacity(this->opacityFactor1);
                if (SUCCEEDED(hr)) {

                    D2D1_RECT_F rectangle1 = D2D1::RectF(
                        (windowSize.right - windowSize.left) - 23,
                        (windowSize.bottom - windowSize.top) - 23,
                        (windowSize.right - windowSize.left) - 5,
                        (windowSize.bottom - windowSize.top) - 5
                    );
                    this->activeArea.bottom = rectangle1.bottom;
                    this->activeArea.top = rectangle1.top;
                    this->activeArea.left = rectangle1.left;
                    this->activeArea.right = rectangle1.right;
                    D2D1_POINT_2F p1 = D2D1::Point2F((windowSize.right - windowSize.left) - 10, (windowSize.bottom - windowSize.top) - 18);
                    D2D1_POINT_2F p2 = D2D1::Point2F((windowSize.right - windowSize.left) - 18, (windowSize.bottom - windowSize.top) - 10);
                    D2D1_POINT_2F p3 = D2D1::Point2F((windowSize.right - windowSize.left) - 18, (windowSize.bottom - windowSize.top) - 18);
                    D2D1_POINT_2F p4 = D2D1::Point2F((windowSize.right - windowSize.left) - 10, (windowSize.bottom - windowSize.top) - 10);
                    m_pRenderTarget->FillRectangle(&rectangle1, m_pDarkGreyBrush);
                    m_pRenderTarget->DrawLine(p1, p2, m_pWhiteBrush);
                    m_pRenderTarget->DrawLine(p3, p4, m_pWhiteBrush);

                    SafeRelease(&m_pDarkGreyBrush);
                    SafeRelease(&m_pWhiteBrush);
                    return true;
                }
                else {
                    SafeRelease(&m_pDarkGreyBrush);
                    SafeRelease(&m_pWhiteBrush);
                    return false;
                }
            }
            else {
                SafeRelease(&m_pWhiteBrush);
                return false;
            }
        }
        else return false;
    }
    else return false;
}
bool UIelement::drawImage(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    bool result = false;

    IWICBitmapDecoder* pDecoder = NULL;
    IWICBitmapFrameDecode* pSource = NULL;
    IWICStream* pStream = NULL;
    IWICFormatConverter* pConverter = NULL;
    IWICBitmapScaler* pScaler = NULL;

    std::wstring widestr = std::wstring(adress.begin(), adress.end());
    //widestr.c_str()
    HRESULT hr = pIWICFactory->CreateDecoderFromFilename(
        widestr.c_str(),
        NULL,
        GENERIC_READ,
        WICDecodeMetadataCacheOnLoad,
        &pDecoder
    );
    if (SUCCEEDED(hr)) {
        // Create the initial frame.
        hr = pDecoder->GetFrame(0, &pSource);
    }
    if (SUCCEEDED(hr)) {

        // Convert the image format to 32bppPBGRA
        // (DXGI_FORMAT_B8G8R8A8_UNORM + D2D1_ALPHA_MODE_PREMULTIPLIED).
        hr = pIWICFactory->CreateFormatConverter(&pConverter);

    }
    if (SUCCEEDED(hr)) {
        hr = pConverter->Initialize(
            pSource,
            GUID_WICPixelFormat32bppPBGRA,
            WICBitmapDitherTypeNone,
            NULL,
            0.f,
            WICBitmapPaletteTypeMedianCut
        );
    }
    if (SUCCEEDED(hr)) {

        // Create a Direct2D bitmap from the WIC bitmap.
        hr = m_pRenderTarget->CreateBitmapFromWicBitmap(
            pConverter,
            NULL,
            &this->image
        );
    }
    if (SUCCEEDED(hr)) {
        D2D1_SIZE_F a;
        if ((this->size.x == 0) && (this->size.y == 0)) {
            a = this->image->GetSize();
        }
        else {
            a.width = this->size.x;
            a.height = this->size.y;
        }

        D2D1_RECT_F size = D2D1::RectF(
            this->position.x,
            this->position.y,
            this->position.x + a.width,
            this->position.y + a.height
        );

        m_pRenderTarget->DrawBitmap(
            this->image,
            size
        );
        result = true;
    }

    RECT area;
    area.left = this->position.x;
    area.top = this->position.y;
    area.right = this->position.x + this->size.x;
    area.bottom = this->position.y + this->size.y;
    this->activeArea = area;

    SafeRelease(&pDecoder);
    SafeRelease(&pSource);
    SafeRelease(&pStream);
    SafeRelease(&pConverter);
    SafeRelease(&pScaler);
    SafeRelease(&this->image);

    return result;
}
bool UIelement::drawImageFromHandle(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    bool result = false;
    DWORD error2 = GetLastError();

    IWICBitmapDecoder* pDecoder = NULL;
    IWICBitmapFrameDecode* pSource = NULL;
    IWICStream* pStream = NULL;
    void* pImageFile = NULL;
    IWICFormatConverter* pConverter = NULL;
    IWICBitmapScaler* pScaler = NULL;
    DWORD imageFileSize = 0;


    //HRSRC imageResHandle = FindResourceW(
    //    NULL,
    //    MAKEINTRESOURCEW(this->handleElement),
    //    MAKEINTRESOURCEW(this->handleElement)
    //);

    // Load the resource to the HGLOBAL.
    HGLOBAL imageResDataHandle = LoadImageA(
        GetModuleHandleA(NULL),
        MAKEINTRESOURCEA(this->handleElement),
        IMAGE_BITMAP,
        0,
        0,
        LR_SHARED
    );

    HRESULT hr = (imageResDataHandle ? S_OK : E_FAIL);
    // Lock the resource to retrieve memory pointer.
    if (SUCCEEDED(hr)) {
        pImageFile = LockResource(imageResDataHandle);
        hr = (pImageFile ? S_OK : E_FAIL);
    }
    //// Calculate the size.
    //if (SUCCEEDED(hr)) {
    //    DWORD len = 0;
    //    GetFileSize(imageResDataHandle, &len);
    //    imageFileSize = len;
    //    hr = (imageFileSize ? S_OK : E_FAIL);
    //}

    //Calculate the size.
    HRSRC imageResHandle = NULL;
    if (SUCCEEDED(hr)) {
        imageResHandle = FindResourceW(
            NULL,
            MAKEINTRESOURCEW(this->handleElement),
            RT_BITMAP
        );
        //LoadResource(resourceHR);
        hr = (imageResHandle ? S_OK : E_FAIL);
    }
    if (SUCCEEDED(hr)) {
        DWORD error = GetLastError();
        imageFileSize = SizeofResource(
            GetModuleHandle(NULL),
            imageResHandle
        );
        error = GetLastError();
        hr = (imageFileSize ? S_OK : E_FAIL);
        int bruh = 0;
    }
    // Create a WIC stream to map onto the memory.
    if (SUCCEEDED(hr)) {
        hr = this->pIWICFactory->CreateStream(&pStream);
        DWORD error = GetLastError();
        int bruh = 0;
    }
    // Initialize the stream with the memory pointer and size.
    if (SUCCEEDED(hr)) {
        hr = pStream->InitializeFromMemory(
            reinterpret_cast<BYTE*>(pImageFile),
            imageFileSize);
        DWORD error = GetLastError();
        int bruh = 0;
    }
    if (SUCCEEDED(hr)) {
        // BUG HERE : MEMORY ACCESS VIOLATION (from wrong pStream I guess. GetLastError() doesn't show anything from this whole function).
        hr = this->pIWICFactory->CreateDecoderFromStream(
            pStream,
            NULL,
            WICDecodeMetadataCacheOnLoad,
            &pDecoder
        );
        DWORD error = GetLastError();
        int bruh = 0;
    }
    //if (SUCCEEDED(hr)) {
    //        //hr = this->pIWICFactory->CreateDecoderFromFileHandle(
    //        //    fileHandler,
    //        //    NULL,
    //        //    WICDecodeMetadataCacheOnLoad,
    //        //    &pDecoder
    //        //);
    //    std::wstring name = L"#" + to_wstring(this->handleElement);
    //    const wchar_t* nameW = name.c_str();
    //    hr = this->pIWICFactory->CreateDecoderFromFilename(
    //            nameW,
    //            NULL,
    //            GENERIC_ALL,
    //            WICDecodeMetadataCacheOnLoad,
    //            &pDecoder
    //        );
    //    }

    if (SUCCEEDED(hr)) {
        // Create the initial frame.
        hr = pDecoder->GetFrame(0, &pSource);
    }
    if (SUCCEEDED(hr)) {

        // Convert the image format to 32bppPBGRA
        // (DXGI_FORMAT_B8G8R8A8_UNORM + D2D1_ALPHA_MODE_PREMULTIPLIED).
        hr = this->pIWICFactory->CreateFormatConverter(&pConverter);

    }
    if (SUCCEEDED(hr)) {
        hr = pConverter->Initialize(
            pSource,
            GUID_WICPixelFormat32bppPBGRA,
            WICBitmapDitherTypeNone,
            NULL,
            0.f,
            WICBitmapPaletteTypeMedianCut
        );
    }
    if (SUCCEEDED(hr)) {

        // Create a Direct2D bitmap from the WIC bitmap.
        hr = m_pRenderTarget->CreateBitmapFromWicBitmap(
            pConverter,
            NULL,
            &this->image
        );
    }
    if (SUCCEEDED(hr)) {
        D2D1_SIZE_F a;
        a = this->image->GetSize();

        D2D1_RECT_F size = D2D1::RectF(
            50,
            50,
            50 + a.width,
            50 + a.height
        );

        m_pRenderTarget->DrawBitmap(
            this->image,
            size
        );
        result = true;
    }

    SafeRelease(&pDecoder);
    SafeRelease(&pSource);
    SafeRelease(&pStream);
    SafeRelease(&pConverter);
    SafeRelease(&pScaler);
    SafeRelease(&this->image);
    return result;
}
bool UIelement::drawText(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    HRESULT hr;

    ID2D1SolidColorBrush* m_pColorBrush;
    hr = m_pRenderTarget->CreateSolidColorBrush(
        D2D1::ColorF(colorValue, this->opacityFactor1),
        &m_pColorBrush
    );
    int i = 0;
    std::wstring txt = std::wstring(this->text.begin(), this->text.end());
    const wchar_t* sc_text;
    sc_text = txt.c_str();
    for (i; sc_text[i] != NULL; i++);
    if (SUCCEEDED(hr)) {
        float sizeOfText = 0;
        if (this->fvalue1 == 0) sizeOfText = 13.0f;
        else sizeOfText = this->fvalue1;
        hr = m_pD2dWriteFactory->CreateTextFormat(
            L"Gabriola",
            NULL,
            DWRITE_FONT_WEIGHT_NORMAL,
            DWRITE_FONT_STYLE_NORMAL,
            DWRITE_FONT_STRETCH_NORMAL,
            sizeOfText,
            L"", //locale
            &this->textFormat
        );
    }
    if (SUCCEEDED(hr)) {
        m_pRenderTarget->DrawText(
            sc_text,
            i,
            textFormat,
            RECTtoRECTF(this->getArea()),
            m_pColorBrush
        );
    }

    SafeRelease(&m_pColorBrush);

    if (SUCCEEDED(hr)) {
        return true;
    }
    return false;
}
bool UIelement::drawTextProgressively(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    HRESULT hr;

    ID2D1SolidColorBrush* m_pColorBrush;
    hr = m_pRenderTarget->CreateSolidColorBrush(
        D2D1::ColorF(D2D1::ColorF(colorValue)),
        &m_pColorBrush
    );
    if (this->_shouldWrite) {
        if (this->bool1) this->clockValue1 = std::chrono::system_clock::now() + (this->timeOffset.time_since_epoch() % this->incrementor1);
        std::chrono::time_point now = std::chrono::system_clock::now();
        if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
            while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
                this->value1++;
                this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
            }
            this->clockValue1 = std::chrono::system_clock::now();
        }
    }
    else {
        this->bool1 = true;
    }

    bool test = true;
    while (test) {
        if (this->text.length() >= this->value1) {
            if (this->text[this->value1] == ' ') this->value1++;
            else test = false;
        }
        else test = false;
    }

    string bruh;
    int o;
    for (o = 0; (o < this->value1) && (o < this->text.size()); o++) {
        bruh = bruh + this->text[o];
    }
    if (o == this->text.size()) {
        this->_jobDone = true;
    }
    this->characterPosition = o;
    std::wstring txt = std::wstring(bruh.begin(), bruh.end());
    const wchar_t* sc_text;
    sc_text = txt.c_str();
    int i = 0;
    for (i; sc_text[i] != NULL; i++);

    this->size.x = this->size.y * o;
    if (SUCCEEDED(hr)) {
        hr = m_pD2dWriteFactory->CreateTextFormat(
            L"Gabriola",
            NULL,
            DWRITE_FONT_WEIGHT_NORMAL,
            DWRITE_FONT_STYLE_NORMAL,
            DWRITE_FONT_STRETCH_NORMAL,
            size.y,
            L"", //locale
            &this->textFormat
        );
    }
    if (SUCCEEDED(hr)) {
        RECT a = this->getArea();
        a.top = a.top - (this->size.y / 1.5);
        a.bottom = a.bottom - (this->size.y / 1.5);
        m_pRenderTarget->DrawText(
            sc_text,
            i,
            textFormat,
            RECTtoRECTF(a),
            m_pColorBrush
        );
    }

    SafeRelease(&m_pColorBrush);

    if (SUCCEEDED(hr)) {
        return true;
    }
    return false;
}
bool UIelement::drawPixelizedBlackFrame(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    if (this->opacityFactor1 == 0) return true;
    RECT windowSize;
    RECT effectArea;
    if (GetWindowRect(m_hwnd, &windowSize)) {
        HRESULT hr;
        ID2D1SolidColorBrush* m_pBrush;
        hr = m_pRenderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF(this->colorValue)),
            &m_pBrush
        );
        while (this->opacityFactor1 > 1) this->opacityFactor1 = this->opacityFactor1 / 10;
        if (this->value1 < 1) this->value1 = 1;
        if (SUCCEEDED(hr)) {
            if (this->size.x != 0) {
                effectArea.left = this->position.x;
                effectArea.right = this->position.x + size.x;
            }
            else {
                effectArea.left = 0;
                effectArea.right = windowSize.right - windowSize.left;
            }
            if (this->size.y != 0) {
                effectArea.top = this->position.y;
                effectArea.bottom = this->position.y + size.y;
            }
            else {
                effectArea.top = 0;
                effectArea.bottom = windowSize.bottom - windowSize.top;
            }
            int a = 0, b = 0;
            RECT pixel;
            pixel.left = (a * this->value1) + effectArea.left;
            pixel.right = pixel.left + this->value1;
            pixel.top = (b * this->value1) + effectArea.top;
            pixel.bottom = pixel.top + this->value1;
            while (pixel.top <= effectArea.bottom) {
                while (pixel.left <= effectArea.right) {
                    m_pBrush->SetOpacity(randomF(0, this->opacityFactor1));
                    m_pRenderTarget->FillRectangle(RECTtoRECTF(pixel), m_pBrush);
                    a++;
                    pixel.left = (a * this->value1) + effectArea.left;
                    pixel.right = pixel.left + this->value1;
                }
                b++;
                a = 0;
                pixel.top = (b * this->value1) + effectArea.top;
                pixel.bottom = pixel.top + this->value1;
                pixel.left = (a * this->value1) + effectArea.left;
                pixel.right = pixel.left + this->value1;
            }
            return true;
        }
        else return false;
    }
    return false;
}
bool UIelement::drawAnimation(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget, int anim) {
    this->generateRectangleCollisionSection();
    return this->owner->drawAnimation(m_hwnd, m_pRenderTarget, anim, this->value1);
}

// Visual Novel Effects :
bool UIelement::displayTextVN1(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {

}

// RENDERTARGET TRANSFORMATION EFFECTS FUNCTIONS :
bool UIelement::renderTargetTransformationReset(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
    return true;
}
bool UIelement::renderTargetTransformationCylinderEffect(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    //m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Scale(1, 1, D2D1::Point2F(1000,500)));
    return true;
}

RECT UIelement::getArea() {
    RECT a;
    a.left = this->position.x;
    a.right = this->position.x + this->size.x;
    a.top = this->position.y;
    a.bottom = this->position.y + this->size.y;
    return a;
}

void UIelement::rePosition(POINT position, unsigned int alignment) {
    this->alignement = alignment;
    this->position = position;
}
void UIelement::rePosition(int positionX, int positionY, unsigned int alignment) {
    this->alignement = alignment;
    this->position.x = positionX;
    this->position.y = positionY;
}

void UIelement::reRelativePosition(POINT position) {
    this->position.x = this->position.x + position.x;
    this->position.y = this->position.y + position.y;
}
void UIelement::reRelativePosition(int positionX, int positionY) {
    this->position.x = this->position.x + positionX;
    this->position.y = this->position.y + positionY;
}

void UIelement::setNewRotationValueMenuRPG1(int val) {
    this->value3 = this->value1;
    this->value1 = val;
    this->clockValue1 = std::chrono::system_clock::now();
}

void UIelement::hide() {
    this->_mustDisplay = false;
}
void UIelement::show() {
    this->_mustDisplay = true;
}
bool UIelement::_isDisplayed() {
    return this->_mustDisplay;
}

void UIelement::startWriting() {
    this->_shouldWrite = true;
    this->timeOffset = this->timeOffset - std::chrono::system_clock::now().time_since_epoch();
    if (this->timeOffset.time_since_epoch() < std::chrono::seconds(0)) this->timeOffset = std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch();
}

void UIelement::stopWriting() {
    this->_shouldWrite = false;
    this->timeOffset = std::chrono::system_clock::now();
}

HRESULT UIelement::CreateDeviceIndependentResources() {
    HRESULT hr = S_OK;

    // Create a Direct2D factory.
    hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &this->m_pDirect2dFactory);
    if (SUCCEEDED(hr))
    {
        hr = DWriteCreateFactory(
            DWRITE_FACTORY_TYPE_SHARED,
            __uuidof(IDWriteFactory),
            reinterpret_cast<IUnknown**>(&this->m_pD2dWriteFactory)
        );
    }
    if (SUCCEEDED(hr)) {
        // Create a WIC imaging factory.
        hr = CoCreateInstance(
            CLSID_WICImagingFactory,
            NULL,
            CLSCTX_INPROC_SERVER,
            IID_PPV_ARGS(&pIWICFactory)
        );
    }
    return hr;
}

void UIelement::resize(POINT size) {
    this->size = size;
}
void UIelement::resize(int sizeX, int sizeY) {
    POINT size;
    size.x = sizeX;
    size.y = sizeY;
    this->size = size;
}

bool UIelement::check_isHovered(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget, POINT cursor) {
    if (this->activeArea.right < this->activeArea.left) {
        long inter = this->activeArea.right;
        this->activeArea.right = this->activeArea.left;
        this->activeArea.left = inter;
    }
    if (this->activeArea.bottom < this->activeArea.top) {
        long inter = this->activeArea.bottom;
        this->activeArea.bottom = this->activeArea.top;
        this->activeArea.top = inter;
    }
    switch (this->style) {

    default:
        if ((cursor.x >= this->activeArea.left) && (cursor.x <= this->activeArea.right)) {
            if ((cursor.y >= this->activeArea.top) && (cursor.y <= this->activeArea.bottom)) {
                return true;
            }
            else return false;
        }
        else return false;
    }
}

void UIelement::init() {
    this->clockValue1 = std::chrono::system_clock::now();
    this->clockValue2 = std::chrono::system_clock::now();
    this->_mustDisplay = true;
    HRESULT hr;
    hr = CreateDeviceIndependentResources();
    if (SUCCEEDED(hr))
    {
        // Create a DirectWrite text format object.
        hr = m_pD2dWriteFactory->CreateTextFormat(
            L"Gabriola",
            NULL,
            DWRITE_FONT_WEIGHT_NORMAL,
            DWRITE_FONT_STYLE_NORMAL,
            DWRITE_FONT_STRETCH_NORMAL,
            25.0f,
            L"", //locale
            &MainMenuMainButtonsTextFormat
        );
    }
    if (SUCCEEDED(hr))
    {
        // Center the text horizontally and vertically.
        MainMenuMainButtonsTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
        MainMenuMainButtonsTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
    }
}

void UIelement::affectWindowStyle(DWORD windowStyle) {
    this->windowStyle = windowStyle;
}

void UIelement::setHandleElement(DWORD handle) {
    this->handleElement = handle;
}

/// <summary>
/// Declare : {alignement, position}
/// </summary>
UIelement::UIelement() {
    this->init();
}
/// <summary>
/// Declare : {alignement, position}
/// </summary>
UIelement::UIelement(POINT position, unsigned int alignement) {
    this->init();
    this->alignement = alignement;
    this->position = position;
}
UIelement::~UIelement() {

}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * MENU * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


bool UIcollection::displayElements(HWND m_hwnd) {
    return false;
}
void UIcollection::affectWindow(HWND m_hwnd) {
    this->m_Hwnd = m_hwnd;
}
UIelement UIcollection::getElement(int i) {
    return elements[i];
}
UIelement* UIcollection::getElementPointer(int i) {
    return &this->elements[i];
}
void UIcollection::addElement(UIelement element) {
    int i = 0;
    element.owner = this;
    for (i; !this->elements[i].get_isVoid(); i++);
    this->elements[i] = element;
    this->elements[i].affectWindowStyle(this->windowStyle);
}
void UIcollection::insertElement(UIelement element, int a) {
    element.owner = this;
    UIelement tab[maxElementsInMenu];
    for (int i = a; i < maxElementsInMenu; i++) {
        tab[i] = this->elements[i];
    }
    for (int i = a; !elements[i].get_isVoid() && i < (maxElementsInMenu - 1); i++) {
        elements[i + 1] = tab[i];
    }
    elements[a] = element;
    this->elements[a].affectWindowStyle(this->windowStyle);
}
void UIcollection::removeElement(int a) {
    for (int i = a; i < (maxElementsInMenu - 1); i++) {
        this->elements[i] = elements[i + 1];
    }
}

bool UIcollection::drawElements(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
    if (this->_mustDisplay) {
        if (this->_drawBg) m_pRenderTarget->Clear(m_pBgBrush->GetColor());
        int i = 0;
        for (i; i < maxElementsInMenu; i++) {
            if (elements[i]._mustDisplay) {
                elements[i].drawElement(m_hwnd, m_pRenderTarget);
            }
        }
        return true;
    }
    else return false;
}

/// <summary>
///  Check wether or not each UIelements are situated below the POINT in argument.
///  For every element detected, the next element within the array "isHovered[]"
///  gets its number, based on its position within "elements[]".
/// </summary>
bool UIcollection::checkIfHovered(POINT cursor) {
    for (int i = 0; i < maxElementsInMenu; i++) this->_isHovered[i] = -1;
    bool _isHovered = false;
    int y = 0;
    for (int i = 0; i < maxElementsInMenu; i++) {
        if (this->elements[i].check_isHovered(this->m_Hwnd, this->m_pRenderTarget, cursor)) {
            _isHovered = true;
            this->_isHovered[y] = i;
            y++;
        }
    }
    return _isHovered;
}
void UIcollection::setBgBrush(ID2D1SolidColorBrush* m_pBgBrush) {
    this->m_pBgBrush = m_pBgBrush;
}

void UIcollection::refreshElementsOwner() {
    int i = 0;
    for (i; !this->elements[i].get_isVoid(); i++) {
        elements[i].owner = this;
    }
}

void UIcollection::hide() {
    this->_mustDisplay = false;
}
void UIcollection::show() {
    this->_mustDisplay = true;
}
bool UIcollection::isDisplayed() {
    return this->_mustDisplay;
}
void UIcollection::_doDrawBg(bool _drawBg) {
    this->_drawBg = _drawBg;
}

void UIcollection::popText(std::string text) {
    RECT windowSize;
    if (GetWindowRect(this->m_Hwnd, &windowSize)) {
        UIelement popup;
        popup.reStyle(3);
        popup.reText(text);
        popup.show();
        this->elements[498] = popup;
        UIelement closePopupButton;
        closePopupButton.reStyle(4);
        closePopupButton.opacityFactor1 = 1;
        closePopupButton.show();
        this->elements[499] = closePopupButton;
    }
}

void UIcollection::popMoreText(std::string text) {
    RECT windowSize;
    if (GetWindowRect(this->m_Hwnd, &windowSize)) {
        UIelement popup;
        popup.reStyle(3);
        string moreText = this->elements[498].getText() + ' ' + text;
        popup.reText(text);
        popup.show();
        this->elements[498] = popup;
        UIelement closePopupButton;
        closePopupButton.reStyle(4);
        closePopupButton.opacityFactor1 = 1;
        closePopupButton.show();
        this->elements[499] = closePopupButton;
    }
}

void UIcollection::init(ID2D1HwndRenderTarget* m_pRenderTarget) {
    HRESULT hr;
    for (int i = 0; i < maxElementsInMenu; i++) this->elements[i].re_isVoid(true);
    for (int i = 0; i < maxElementsInMenu; i++) this->_isHovered[i] = -1;

    hr = m_pRenderTarget->CreateSolidColorBrush(
        D2D1::ColorF(D2D1::ColorF(0x000000)),
        &m_pBgBrush
    );
    this->_mustDisplay = true;
    this->owner = 0;
}
void UIcollection::init(ID2D1HwndRenderTarget* m_pRenderTarget, HWND m_Hwnd) {
    this->m_Hwnd = m_Hwnd;
    this->init(m_pRenderTarget);
}

void UIcollection::affectWindowStyle(DWORD windowStyle) {
    this->windowStyle = windowStyle;
}


void UIcollection::moveRelativePosition(POINT position) {
    int i = 0;
    while (i < maxElementsInMenu && !this->elements[i].get_isVoid()) {
        this->elements[i].reRelativePosition(position);
        i++;
    }
}
void UIcollection::moveRelativePosition(int x, int y) {
    int i = 0;
    while (i < maxElementsInMenu && !this->elements[i].get_isVoid()) {
        this->elements[i].reRelativePosition(x, y);
        i++;
    }
}

bool UIcollection::animateCollection(int collection, RECT activeZone, int speed, int pos, bool _insert) {
    UIelement animationMarker;
    animationMarker.value1 = speed;
    animationMarker.reType(2);
    animationMarker.reStyle(collection);
    animationMarker.rePosition(activeZone.left, activeZone.top);
    animationMarker.resize(activeZone.right - activeZone.left, activeZone.bottom - activeZone.top);
    animationMarker.generateRectangleCollisionSection();
    if ((collection >= maxAnimationsInUIcollection) || (collection < 0)) {
        return false;
    }
    else {
        this->clockValues[collection] = std::chrono::system_clock::now();
        this->_animate[collection] = true;
        if ((pos < 0) || (pos > maxElementsInMenu)) {
            this->addElement(animationMarker);
        }
        else {
            if (_insert) {
                this->insertElement(animationMarker, pos);
            }
            else {
                this->removeElement(pos);
                this->insertElement(animationMarker, pos);
            }
        }
        return true;
    }
}


bool UIcollection::addAnimationElement(UIelement element, int collection) {
    int watch = collection;
    if (collection == 0) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation0[i].get_isVoid()) end = true;
        }
        animation0[i] = element;
    }
    if (collection == 1) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation1[i].get_isVoid()) end = true;
        }
        animation1[i] = element;
    }
    if (collection == 2) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation2[i].get_isVoid()) end = true;
        }
        animation2[i] = element;
    }
    if (collection == 3) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation3[i].get_isVoid()) end = true;
        }
        animation3[i] = element;
    }
    if (collection == 4) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation4[i].get_isVoid()) end = true;
        }
        animation4[i] = element;
    }
    if (collection == 5) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation5[i].get_isVoid()) end = true;
        }
        animation5[i] = element;
    }
    if (collection == 6) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation6[i].get_isVoid()) end = true;
        }
        animation6[i] = element;
    }
    if (collection == 7) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation7[i].get_isVoid()) end = true;
        }
        animation7[i] = element;
    }
    if (collection == 8) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation8[i].get_isVoid()) end = true;
        }
        animation8[i] = element;
    }
    if (collection == 9) {
        bool end = false;
        int i = -1;
        while ((i < maxUIelementsInAnimation) && !end) {
            i++;
            if (this->animation9[i].get_isVoid()) end = true;
        }
        animation9[i] = element;
    }
    return true;
}
void UIcollection::deleteAnimation(int collection) {
    UIelement empty;
    empty.re_isVoid(true);
    if (collection == -1) {
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation0[i] = empty;
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation1[i] = empty;
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation2[i] = empty;
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation3[i] = empty;
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation4[i] = empty;
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation5[i] = empty;
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation6[i] = empty;
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation7[i] = empty;
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation8[i] = empty;
        for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation9[i] = empty;
    }
    if (collection == 0) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation0[i] = empty;
    if (collection == 1) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation1[i] = empty;
    if (collection == 2) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation2[i] = empty;
    if (collection == 3) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation3[i] = empty;
    if (collection == 4) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation4[i] = empty;
    if (collection == 5) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation5[i] = empty;
    if (collection == 6) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation6[i] = empty;
    if (collection == 7) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation7[i] = empty;
    if (collection == 8) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation8[i] = empty;
    if (collection == 9) for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation9[i] = empty;
}

// TODO : Fonction au dessus qui n'enregistre pas animation[1+]. Fonction au dessous pour laquelle pointed ne prends mm pas la valeur 0 d'animation.

bool UIcollection::drawAnimation(HWND m_hwnd, ID2D1HwndRenderTarget* m_pRenderTarget, int collection, int speed) {
    if (collection == 0) {
        if (!this->_animate[collection]) {
            return this->animation0[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation0[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation0[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    if (collection == 1) {
        if (!this->_animate[collection]) {
            return this->animation1[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation1[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation1[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    if (collection == 2) {
        if (!this->_animate[collection]) {
            return this->animation2[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation2[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation2[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    if (collection == 3) {
        if (!this->_animate[collection]) {
            return this->animation3[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation3[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation3[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    if (collection == 4) {
        if (!this->_animate[collection]) {
            return this->animation4[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation4[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation4[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    if (collection == 5) {
        if (!this->_animate[collection]) {
            return this->animation5[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation5[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation5[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    if (collection == 6) {
        if (!this->_animate[collection]) {
            return this->animation6[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation6[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation6[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    if (collection == 7) {
        if (!this->_animate[collection]) {
            return this->animation7[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation7[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation7[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    if (collection == 8) {
        if (!this->_animate[collection]) {
            return this->animation8[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation8[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation8[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    if (collection == 9) {
        if (!this->_animate[collection]) {
            return this->animation9[0].drawImage(m_hwnd, m_pRenderTarget);
        }

        int amountOfElements = 0;
        for (amountOfElements; this->animation9[amountOfElements].get_isVoid() == false; amountOfElements++);

        std::chrono::time_point difference = std::chrono::system_clock::now() - this->clockValues[collection].time_since_epoch();

        int imageToDraw = 0;
        std::chrono::duration spedo = std::chrono::milliseconds(speed);


        while (difference > (std::chrono::system_clock::now() - std::chrono::system_clock::now().time_since_epoch())) { // compare difference to a zero
            difference = difference - spedo;
            imageToDraw++;
        }

        if (imageToDraw > 0) imageToDraw--;

        //std::chrono::time_point now = std::chrono::system_clock::now();
        //if (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //    while (now >= (this->clockValue1 + std::chrono::milliseconds(this->incrementor1))) {
        //        this->value1++;
        //        this->clockValue1 = this->clockValue1 + std::chrono::milliseconds(this->incrementor1);
        //    }
        //    this->clockValue1 = std::chrono::system_clock::now();
        //}

        imageToDraw = imageToDraw % amountOfElements;

        if (this->_stopAnimations[collection] && imageToDraw == 0) {
            this->_animate[collection] = false;
            this->_stopAnimations[collection] = false;
        }
        return this->animation9[imageToDraw].drawImage(m_hwnd, m_pRenderTarget);
    }
    return false;
}

void UIcollection::initAnim() {
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation0[i].re_isVoid(true);
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation1[i].re_isVoid(true);
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation2[i].re_isVoid(true);
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation3[i].re_isVoid(true);
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation4[i].re_isVoid(true);
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation5[i].re_isVoid(true);
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation6[i].re_isVoid(true);
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation7[i].re_isVoid(true);
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation8[i].re_isVoid(true);
    for (int i = 0; i < maxUIelementsInAnimation; i++) this->animation9[i].re_isVoid(true);
}

/// <summary>
/// Declare {m_hwnd, m_pRenderTarget} {m_pBackGroundBrush} {windowStyle}
/// </summary>
UIcollection::UIcollection(HWND m_Hwnd, ID2D1HwndRenderTarget* m_pRenderTarget, DWORD windowStyle) {
    this->init(m_pRenderTarget);
    this->m_Hwnd = m_Hwnd;
    this->m_pRenderTarget = m_pRenderTarget;
    this->windowStyle = windowStyle;
    initAnim();
}
/// <summary>
/// Declare {m_hwnd, m_pRenderTarget} {m_pBackGroundBrush} {windowStyle}
/// </summary>
UIcollection::UIcollection(HWND m_Hwnd, ID2D1HwndRenderTarget* m_pRenderTarget, ID2D1SolidColorBrush* m_pBgBrush, DWORD windowStyle) {
    this->init(m_pRenderTarget);
    this->m_pBgBrush = m_pBgBrush;
    this->m_Hwnd = m_Hwnd;
    this->m_pRenderTarget = m_pRenderTarget;
    this->windowStyle = windowStyle;
    initAnim();
}
/// <summary>
/// Declare {m_hwnd, m_pRenderTarget} {m_pBackGroundBrush} {windowStyle}
/// </summary>
UIcollection::UIcollection() {
    initAnim();
}

UIcollection::~UIcollection() {
    SafeRelease(&this->m_pBgBrush);
}