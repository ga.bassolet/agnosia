#include "GeneralFunctions.h"


using namespace std;

string textToScreenSize(string text, int speed) {
	return text;
}
string outStat(string name, const wchar_t pathToFile[]) {
	std::ifstream bruh(pathToFile);
	if (bruh.is_open()) {
		string fileText(istreambuf_iterator<char>(bruh), {});
		string inter;
		int start, x;

		start = fileText.find(name) + name.length();
		inter = fileText;
		//cout << endl << endl << endl << "first inter for " << name << " : " << " = " << inter;
		x = inter.length();
		bool overflowed = false;
		for (int i = 0; i < x; i++) {
			if ((i + start) == x && !overflowed) {
				overflowed = true;
				inter.erase(i, x);
				//cout << endl << "inter erased " << i << " time(s). inter = " << inter;
			}
			if (!overflowed) {
				inter[i] = inter[i + start];
			}
		}
		x = inter.find_first_of('\n');
		inter.erase(x, string::npos);
		//cout << endl << "final inter for " << name << " = " << inter;
		return inter;
	}
}
string outStat(string name, string text) {
	string inter;
	int start, x;

	start = text.find(name) + name.length();
	inter = text;
	//cout << endl << endl << endl << "first inter for " << name << " : " << " = " << inter;
	x = inter.length();
	bool overflowed = false;
	for (int i = 0; i < x; i++) {
		if ((i + start) == x && !overflowed) {
			overflowed = true;
			inter.erase(i, x);
			//cout << endl << "inter erased " << i << " time(s). inter = " << inter;
		}
		if (!overflowed) {
			inter[i] = inter[i + start];
		}
	}
	x = inter.find_first_of('\n');
	inter.erase(x, string::npos);
	//cout << endl << "final inter for " << name << " = " << inter;
	return inter;
}
int random(int min, int max) {
	srand(std::chrono::system_clock::now().time_since_epoch().count());
	int val;
	val = (rand() % ((max + 1) - min)) + min;
	return val;
}
float randomF(float min, float max) {
	srand(std::chrono::system_clock::now().time_since_epoch().count());
	float val;
	val = rand();
	while (val >= 1) {
		val = val / 10;
	}
	val = val * (max - min) + min;
	return val;
}

//returns the value+1; if no chara was found, returns 0;
int getFirstChara(char search, string txt) {
	unsigned int i = 0;
	while ((i <= txt.length()) || (txt[i] != search)) {
		i++;
	}
	if (i > txt.length()) i = 0;
	return i;
}
string tabToString(int* tab, int size) {
	string res;
	for (int i = 0; i < size; i++) {
		res = res + to_string(tab[i]) + ",";
	}
	return res;
}
int* stringToTab(string res) {
	int tab[tabMax];
	bool endflag = false;
	int i = 0;
	while (!endflag) {
		int x = res.find_first_of(',');
		int max = res.length();
		if (x <= 0) {
			endflag = true;
		}
		if (res[0] == '-') {
			endflag = true;
		}
		if (!endflag) {
			int value = 0;
			for (int j = 0; j < x; j++) {
				value = (value * 10) + (res[j] - '0'); //stoi pour un string; atoi pour un char
			}
			tab[i] = value;
			res.erase(0, x + 1);
			i++;
		}
	}
	fillTabFT(tab, -1, i, tabMax - 1);
	return tab;
}
int stringToInt(string res) {
	int value = 0;
	int x = res.length();
	for (int j = 0; j < x; j++) {
		value = (value * 10) + (res[j] - '0'); //stoi pour un string; atoi pour un char
	}
	return value;
}
long stringToLong(string res) {
	long value = 0;
	long x = res.length();
	for (long j = 0; j < x; j++) {
		value = (value * 10) + (res[j] - '0'); //stoi pour un string; atoi pour un char
	}
	return value;
}
string intToString(int val) {
	string result;
	char meh;
	int i = 10;
	while (val > 0) {
		int inter = val % i;
		val = val - inter;
		meh = ('0' + val % i);
		result = result + meh;
		i = i * 10;
	}
	string result2;
	int y = 0;
	for (int i2 = result.length(); i >= 0; i--) {
		result2[y] = result[i];
		y++;
	}
	return result2;
}
string longToString(long val) {
	string result;
	char meh;
	long i = 10;
	while (val > 0) {
		long inter = val % i;
		val = val - inter;
		meh = ('0' + val % i);
		result = result + meh;
		i = i * 10;
	}
	string result2;
	long y = 0;
	for (long i2 = result.length(); i >= 0; i--) {
		result2[y] = result[i];
		y++;
	}
	return result2;
}
string boolToString(bool val) {
	if (val == true) return "true";
	else return "false";
}
void fillTab(int tab[], int val, int size) {
	for (int i = 0; i <= size; i++) {
		tab[i] = val;
	}
}
void fillTabF(int tab[], int val, int from) {
	for (int i = from; i <= tabMax; i++) {
		tab[i] = val;
	}
}
void fillTabFT(int tab[], int val, int from, int to) {
	for (int i = from; i <= to; i++) {
		tab[i] = val;
	}
}

D2D1_RECT_F RECTtoRECTF(RECT rect) {
	D2D1_RECT_F bruh = D2D1::RectF(
		rect.left,
		rect.top,
		rect.right,
		rect.bottom
	);
	return bruh;
}

string statsToBar(int stat, int maxStat, int length) {
	string res;
	length--;
	for (int i = 0; i <= length; i++) {
		if (i <= (stat * length) / maxStat) {
			res = res + '#';
		}
		else {
			res = res + '-';
		}
	}
	return res;
}


long countAmountOfOccurences(std::string text, std::string search) {
	long i = 0;
	bool isFinished = false;
	while (!isFinished) {
		long j = text.find_first_of(search);
		if (j == string::npos) isFinished = true;
		else {
			text.erase(0, j);
			i++;
		}
	}
	return i;
}
long countAmountOfOccurences(std::string text, char search) {
	long i = 0;
	bool isFinished = false;
	while (!isFinished) {
		long j = text.find_first_of(search);
		if (j == string::npos) isFinished = true;
		else {
			text.erase(0, j + 1);
			i++;
		}
	}
	return i;
}

int getSign(float x) {
	if (x < 0) return -1;
	else return 1;
}