#pragma once
// Some libraries won't work unless specified through these commands, even if included.
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "MSVCRTD.lib")
#pragma comment(lib, "Dwrite")
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files:
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <wchar.h>
#include <math.h>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

#include <string>

#include <chrono>
#include <ctime>

#include <iostream>
#include <filesystem>
#include <fstream>
#include <vcruntime.h>

namespace Jinko {

	//Window.h
	class Application;
	class UIcollection;
	class UIelement;
	class RenderTargetEffect;

	//geometry.h
	class vector;
	class point;
	class Ipoint;
	class line;
	class triangle;
	class quad;
}