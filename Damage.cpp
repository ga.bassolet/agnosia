#include "Damage.h"



Agnosia::damage Agnosia::damage::operator+(Agnosia::damage const& A) {
	Agnosia::damage out;
	out.trueDamage = this->trueDamage + A.trueDamage;
	out.magicDamage = this->magicDamage + A.magicDamage;
	out.physicalDamage = this->physicalDamage + A.physicalDamage;

	out.magicPenetration = this->magicPenetration + A.magicPenetration;
	out.physicalPenetration = this->physicalPenetration + A.physicalPenetration;

	return out;
}

void Agnosia::damage::inflict(Agnosia::Mob* mob) {
	float totalDamage = 0;
	float inter;
	if (this->physicalPenetration > 100) this->physicalPenetration = 100.f;
	if (this->physicalPenetration < 0) this->physicalPenetration = 0.f;
	inter = this->physicalDamage - (mob->armor * ((100.f - this->physicalPenetration) / 100.f));
	if (inter >= 0) totalDamage = totalDamage + inter;


	if (this->magicPenetration > 100) this->magicPenetration = 100.f;
	if (this->magicPenetration < 0) this->magicPenetration = 0.f;
	inter = this->magicDamage - (mob->magicResistance * ((100.f - this->magicPenetration) / 100.f));
	if (inter >= 0) totalDamage = totalDamage + inter;

	if (this->trueDamage >= 0) totalDamage = totalDamage + this->trueDamage;

	if (totalDamage >= 0) mob->health = mob->health - totalDamage;
}


Agnosia::damage::damage() {
	this->trueDamage = 0;
	this->magicDamage = 0;
	this->physicalDamage = 0;

	this->magicPenetration = 0;
	this->physicalPenetration = 0;
}